<?php
ini_set('display_errors',1);
ini_set('display_startup_erros',1);
error_reporting(E_ALL);

require_once('../config/config.php');

$login = (isset($_POST['login'])) ? $_POST['login'] : '';
$senha = (isset($_POST['password'])) ? $_POST['password'] : '';

$formMessage = "";
if (isset($_POST['frmLogin'])) {

if (($login == "") || ($senha == "")) {
//header("Location: http://".$_SERVER['SERVER_NAME'].str_replace("authent.php", "", $_SERVER['REQUEST_URI']));
$formMessage = "Senha ou Login incorretos.";
} else {
try {

$sql="SELECT * FROM users_host 
WHERE 
user_login = ? AND 
user_pass = ? AND 
user_status = ?;";

$sth = $dbh->prepare($sql);

$sth->bindParam(1, $login, PDO::PARAM_STR);
$sth->bindValue(2, md5($senha), PDO::PARAM_STR);
$sth->bindValue(3, "1", PDO::PARAM_STR);
$sth->execute();

$totalRegistros = $sth->rowCount();

if ($totalRegistros == "1") {
while($row = $sth->fetch()) {
$_SESSION['userId']       = $row['user_id'];
$_SESSION['userName']     = $row['user_nm'];
$_SESSION['userStatus']   = $row['user_status'];
$_SESSION['occupationId'] = $row['occupation_id'];
if ( $row['user_pass'] == md5("123456")) {
//header("Location: http://".$_SERVER['SERVER_NAME'].str_replace("authent.php", "", $_SERVER['REQUEST_URI'])."changePass");
} else {
//header("Location: http://".$_SERVER['SERVER_NAME'].str_replace("authent.php", "", $_SERVER['REQUEST_URI']));
}

}
//echo "Approved!S";

header("Location: home");
} else {
$formMessage = "Login ou senha incorretos.";
//header("Location: http://".$_SERVER['SERVER_NAME'].str_replace("authent.php", "", $_SERVER['REQUEST_URI']));
}

} catch (PDOException $e) {
print "Error!: " . $e->getMessage() . "<br/>";
die();
}
}
}

?>


<!DOCTYPE html>
<html lang="pt-br">

<!DOCTYPE html>
<!--[if IE 9]>         <html class="no-js lt-ie10" lang="en"> <![endif]-->
<!--[if gt IE 9]><!--> <html class="no-js" lang="pt-br"> <!--<![endif]-->
<head>
<meta charset="utf-8">

<title>Entrada de acesso </title>

<meta name="description" content="ProUI is a Responsive Bootstrap Admin Template created by pixelcave and published on Themeforest.">
<meta name="author" content="pixelcave">
<meta name="robots" content="noindex, nofollow">
<meta name="viewport" content="width=device-width,initial-scale=1.0,user-scalable=0">

<!-- Icons -->
<!-- The following icons can be replaced with your own, they are used by desktop and mobile browsers -->
<link rel="shortcut icon" href="public/img/favicon-matsui.png">
<link rel="apple-touch-icon" href="public/img/favicon-matsui.png" sizes="57x57">
<link rel="apple-touch-icon" href="public/img/favicon-matsui.png" sizes="72x72">
<link rel="apple-touch-icon" href="public/img/favicon-matsui.png" sizes="76x76">
<link rel="apple-touch-icon" href="public/img/favicon-matsui.png" sizes="114x114">
<link rel="apple-touch-icon" href="public/img/favicon-matsui.png" sizes="120x120">
<link rel="apple-touch-icon" href="public/img/favicon-matsui.png" sizes="144x144">
<link rel="apple-touch-icon" href="public/img/favicon-matsui.png" sizes="152x152">
<link rel="apple-touch-icon" href="public/img/favicon-matsui.png" sizes="180x180">
<!-- END Icons -->

<!-- Stylesheets -->
<!-- Bootstrap is included in its original form, unaltered -->
<link rel="stylesheet" href="public/css/bootstrap.min.css">

<!-- Related styles of various icon packs and plugins -->
<link rel="stylesheet" href="public/css/plugins.css">

<!-- The main stylesheet of this template. All Bootstrap overwrites are defined in here -->
<link rel="stylesheet" href="public/css/main.css">

<!-- Include a specific file here from public/css/themes/ folder to alter the default theme of the template -->

<!-- The themes stylesheet of this template (for using specific theme color in individual elements - must included last) -->
<link rel="stylesheet" href="public/css/themes.css">
<!-- END Stylesheets -->

<!-- Modernizr (browser feature detection library) -->
<script src="public/js/vendor/modernizr.min.js"></script>


<script src="public/js/vendor/jquery.min.js"></script>
<script src="public/js/vendor/bootstrap.min.js"></script>
<script src="public/js/plugins.js"></script>
<script src="public/js/app.js"></script>

<!-- Load and execute javascript code used only in this page -->
<script src="public/js/pages/login.js"></script>

<!-- jquery -->
<script type="text/javascript" src="public/js/jquery.js">
</script>

<!-- custom -->
<script type="text/javascript" src="public/js/custom.js">
</script>




</head>




<body>

<!-- Login Alternative Row -->
<div class="container">
<div class="row">
<div class="col-md-5 col-md-offset-1">
<div id="login-alt-container">
<!-- Title -->
<h1 class="push-top-bottom">
<img src="public/img/favicon-matsui.png"> <strong>Matsui Tecnologia</strong><br>

</h1>
<!-- END Title -->



<!-- Footer -->
<footer class="text-muted push-top-bottom">
<small><span></span>Desenvolvido por |  <img src="public/img/favicon-matsui.png" width="10" height="10">  <a href="http://www.matsuitecnologia.com.br" target="_blank">Matsui tecnologia</a></small>
</footer>
<!-- END Footer -->
</div>
</div>
<div class="col-md-5">
<!-- Login Container -->
<div id="login-container">
<!-- Login Title -->
<div class="login-title text-center">
<h1><strong>Entre</strong> com seus <strong>Registros</strong></h1>
</div>
<!-- END Login Title -->

<!-- Login Block -->
<div class="block push-bit">
<!-- Login Form -->



<form class="form-horizontal" role="form" action="login.php" name="frmLogin" method="post">
<input type="hidden" id="frmLogin" name="frmLogin" value="loginForm" >
<div class="alert alert-danger alert-dismissable">
    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
<i class="fa fa-times-circle"></i>
<?php echo $formMessage; ?>  
    
</div>     
<div class="form-group">
<div class="col-xs-12">
<div class="input-group">
<span class="input-group-addon"><i class="gi gi-envelope"></i></span>
<input type="text" id="register-email" name="login" class="form-control input-lg" placeholder="Email">
</div>
</div>
</div>
<div class="form-group">
<div class="col-xs-12">
<div class="input-group">
<span class="input-group-addon"><i class="gi gi-asterisk"></i></span>
<input type="password" id="register-password" name="password" class="form-control input-lg" placeholder="Password">
</div>
</div>
</div>
<div class="form-group form-actions">
<div class="col-xs-4">
<label class="switch switch-primary" data-toggle="tooltip" title="Remember Me?">

</label>
</div>
<div class="col-xs-8 text-right">

<button class="btn btn-sm btn-primary" id=""> Acessar</button>

</div>
</div>
<div class="form-group">
<div class="col-xs-12 text-center">
<a href="javascript:void(0)" id="link-reminder-login"><small>Esqueceu a sua senha ?</small></a> 

</div>
</div>
</form>
<!-- END Login Form -->
</div>
</div>
</div>
</div>
</div>



</body>
</html>
