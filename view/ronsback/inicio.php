<div class="first-slider">
    <div id="rev_slider_211_1_wrapper" class="rev_slider_wrapper fullwidthbanner-container" data-alias="webproductlighthero" style="background-color:#1D4BAD;margin:0px auto;padding:0px;margin-top:0px;margin-bottom:0px;">
        <!-- START REVOLUTION SLIDER 5.1.1RC fullwidth mode -->
        <div id="rev_slider_211_1" class="rev_slider fullwidthabanner" style="display:none;" data-version="5.1.1RC">
            <ul>
                <!-- SLIDE  -->
                <li data-index="rs-699" data-transition="fade" data-slotamount="default" data-easein="default" data-easeout="default" data-masterspeed="1500" data-rotate="0" data-saveperformance="off" data-title="Intro" data-description="">
                    <!-- MAIN IMAGE -->
                    <img src="public/upload/transparent.png" alt="" data-bgposition="center center" data-bgfit="cover" data-bgrepeat="no-repeat" data-bgparallax="5" class="rev-slidebg" data-no-retina>
                    <!-- LAYERS -->

                    <!-- LAYER NR. 1 -->
                    <div class="tp-caption   tp-resizeme rs-parallaxlevel-6" id="slide-699-layer-1" data-x="['right','right','center','center']" data-hoffset="['-254','-453','70','60']" data-y="['middle','middle','middle','bottom']" data-voffset="['30','50','211','25']" data-width="none" data-height="none" data-whitespace="nowrap" data-transform_idle="o:1;" data-transform_in="x:right;s:1500;e:Power3.easeOut;" data-transform_out="opacity:0;s:1500;e:Power4.easeIn;s:1500;e:Power4.easeIn;" data-start="2500" data-responsive_offset="on" style="z-index: 5;"><img src="public/upload/macbookpro.png" alt="" width="1000" height="600" data-ww="['1000px','1000px','500px','350px']" data-hh="['600px','600px','300px','210px']" data-no-retina>
                    </div>

                    <!-- LAYER NR. 2 -->
                    <div class="tp-caption   tp-resizeme rs-parallaxlevel-6 tp-videolayer" id="slide-699-layer-11" data-x="['left','left','left','left']" data-hoffset="['829','866','297','185']" data-y="['top','top','top','top']" data-voffset="['197','217','582','514']" data-whitespace="nowrap" data-transform_idle="o:1;" data-transform_in="z:0;rX:0deg;rY:0;rZ:0;sX:2;sY:2;skX:0;skY:0;opacity:0;s:1500;e:Power3.easeOut;" data-transform_out="opacity:0;s:1500;e:Power4.easeIn;s:1500;e:Power4.easeIn;" data-mask_in="x:0px;y:0px;s:inherit;e:inherit;" data-start="3350" data-responsive_offset="on" data-videocontrols="none" data-videowidth="['653px','653px','325px','229px']" data-videoheight="['408px','408px','204px','145px']" data-videoposter="public/upload/traincover.jpg" data-videomp4="public/upload/Broadway.mp4" data-posterOnMObile="off" data-videopreload="auto" data-videoloop="loop" data-autoplay="on" style="z-index: 6;">
                    </div>

                    <!-- LAYER NR. 3 -->
                    <div class="tp-caption   tp-resizeme rs-parallaxlevel-5" id="slide-699-layer-12" data-x="['left','left','left','left']" data-hoffset="['543','543','110','155']" data-y="['top','top','top','top']" data-voffset="['17','17','500','474']" data-width="none" data-height="none" data-whitespace="nowrap" data-transform_idle="o:1;" data-transform_in="opacity:0;s:300;e:Power2.easeInOut;" data-transform_out="opacity:0;s:300;s:300;" data-start="3950" data-responsive_offset="on" style="z-index: 7;">
                        <div class="rs-looped rs-slideloop" data-easing="Linear.easeNone" data-speed="30" data-xs="-600" data-xe="600" data-ys="0" data-ye="0"><img src="public/upload/cloud1.png" alt="" width="500" height="273" data-ww="['500px','500px','250','125px']" data-hh="['273px','273px','137','68px']" data-no-retina>
                        </div>
                    </div>

                    <!-- LAYER NR. 4 -->
                    <div class="tp-caption   tp-resizeme rs-parallaxlevel-4" id="slide-699-layer-3" data-x="['left','left','center','center']" data-hoffset="['593','633','-110','-60']" data-y="['top','top','top','bottom']" data-voffset="['183','203','590','20']" data-width="none" data-height="none" data-whitespace="nowrap" data-transform_idle="o:1;" data-transform_in="x:right;s:1500;e:Power3.easeOut;" data-transform_out="opacity:0;s:1500;e:Power4.easeIn;s:1500;e:Power4.easeIn;" data-start="2750" data-responsive_offset="on" style="z-index: 8;"><img src="public/upload/ipad.png" alt="" width="430" height="540" data-ww="['430px','430px','200px','170px']" data-hh="['540px','540px','251px','213px']" data-no-retina>
                    </div>

                    <!-- LAYER NR. 5 -->
                    <div class="tp-caption   tp-resizeme rs-parallaxlevel-4" id="slide-699-layer-4" data-x="['left','left','left','center']" data-hoffset="['663','703','212','-60']" data-y="['top','top','top','bottom']" data-voffset="['271','291','632','50']" data-width="none" data-height="none" data-whitespace="nowrap" data-transform_idle="o:1;" data-transform_in="z:0;rX:0deg;rY:0;rZ:0;sX:1.5;sY:1.5;skX:0;skY:0;opacity:0;s:1500;e:Power3.easeOut;" data-transform_out="opacity:0;s:1500;e:Power4.easeIn;s:1500;e:Power4.easeIn;" data-mask_in="x:0px;y:0px;s:inherit;e:inherit;" data-start="3700" data-responsive_offset="on" style="z-index: 9;"><img src="public/upload/express_ipad_content1.jpg" alt="" width="290" height="374" data-ww="['290px','290px','135px','115px']" data-hh="['374px','374px','174px','148px']" data-no-retina>
                    </div>

                    <!-- LAYER NR. 6 -->
                    <div class="tp-caption   tp-resizeme rs-parallaxlevel-3" id="slide-699-layer-13" data-x="['left','left','left','left']" data-hoffset="['294','294','116','97']" data-y="['top','top','top','top']" data-voffset="['532','532','745','641']" data-width="none" data-height="none" data-whitespace="nowrap" data-transform_idle="o:1;" data-transform_in="opacity:0;s:300;e:Power2.easeInOut;" data-transform_out="opacity:0;s:300;s:300;" data-start="3950" data-responsive_offset="on" style="z-index: 10;">
                        <div class="rs-looped rs-slideloop" data-easing="Linear.easeNone" data-speed="30" data-xs="-400" data-xe="400" data-ys="0" data-ye="0"><img src="public/upload/cloud2.png" alt="" width="600" height="278" data-ww="['600px','600px','300','150']" data-hh="['278px','278px','139','70']" data-no-retina>
                        </div>
                    </div>

                    <!-- LAYER NR. 7 -->
                    <div class="tp-caption   tp-resizeme rs-parallaxlevel-2" id="slide-699-layer-5" data-x="['left','left','left','left']" data-hoffset="['530','553','127','58']" data-y="['top','top','top','top']" data-voffset="['278','297','622','529']" data-width="none" data-height="none" data-whitespace="nowrap" data-transform_idle="o:1;" data-transform_in="x:right;s:1500;e:Power3.easeOut;" data-transform_out="opacity:0;s:1500;e:Power4.easeIn;s:1500;e:Power4.easeIn;" data-start="3000" data-responsive_offset="on" style="z-index: 11;"><img src="public/upload/ihpone.png" alt="" width="260" height="450" data-ww="['260px','260px','130px','100px']" data-hh="['450px','450px','225px','173px']" data-no-retina>
                    </div>

                    <!-- LAYER NR. 8 -->
                    <div class="tp-caption   tp-resizeme rs-parallaxlevel-2" id="slide-699-layer-6" data-x="['left','left','left','left']" data-hoffset="['576','598','150','75']" data-y="['top','top','top','top']" data-voffset="['360','379','663','560']" data-width="none" data-height="none" data-whitespace="nowrap" data-transform_idle="o:1;" data-transform_in="z:0;rX:0deg;rY:0;rZ:0;sX:1.5;sY:1.5;skX:0;skY:0;opacity:0;s:1500;e:Power3.easeOut;" data-transform_out="opacity:0;s:1500;e:Power4.easeIn;s:1500;e:Power4.easeIn;" data-mask_in="x:0px;y:0px;s:inherit;e:inherit;" data-start="3950" data-responsive_offset="on" style="z-index: 12;"><img src="public/upload/express_iphone_content1.jpg" alt="" width="170" height="286" data-ww="['170px','170px','85px','66px']" data-hh="['286px','286px','143px','111px']" data-no-retina>
                    </div>

                    <!-- LAYER NR. 9 -->
                    <div class="tp-caption   tp-resizeme rs-parallaxlevel-1" id="slide-699-layer-14" data-x="['left','left','left','left']" data-hoffset="['280','280','-10','-1']" data-y="['top','top','top','top']" data-voffset="['223','223','569','518']" data-width="none" data-height="none" data-whitespace="nowrap" data-transform_idle="o:1;" data-transform_in="opacity:0;s:300;e:Power2.easeInOut;" data-transform_out="opacity:0;s:300;s:300;" data-start="3950" data-responsive_offset="on" style="z-index: 13;">
                        <div class="rs-looped rs-slideloop" data-easing="Linear.easeNone" data-speed="30" data-xs="-200" data-xe="200" data-ys="0" data-ye="0"><img src="public/upload/cloud3.png" alt="" width="738" height="445" data-ww="['600px','600px','300','150']" data-hh="['278px','278px','181','90']" data-no-retina>
                        </div>
                    </div>

                    <!-- LAYER NR. 10 -->
                    <div class="tp-caption WebProduct-Title   tp-resizeme rs-parallaxlevel-7" id="slide-699-layer-7" data-x="['left','left','left','left']" data-hoffset="['30','30','200','80']" data-y="['middle','middle','top','top']" data-voffset="['0','0','177','160']" data-fontsize="['90','90','75','60']" data-lineheight="['90','90','75','60']" data-width="none" data-height="none" data-whitespace="nowrap" data-transform_idle="o:1;" data-transform_in="x:-50px;opacity:0;s:1000;e:Power2.easeOut;" data-transform_out="opacity:0;s:1500;e:Power4.easeIn;s:1500;e:Power4.easeIn;" data-start="1000" data-splitin="none" data-splitout="none" data-responsive_offset="on" style="z-index: 14; white-space: nowrap;">Matsui Cloud
                        <br/>Servers!
                    </div>

                    <!-- LAYER NR. 11 -->
                    <div class="tp-caption WebProduct-Content   tp-resizeme rs-parallaxlevel-7" id="slide-699-layer-9" data-x="['left','left','left','left']" data-hoffset="['30','30','200','80']" data-y="['middle','middle','top','top']" data-voffset="['129','127','365','314']" data-fontsize="['16','16','16','14']" data-lineheight="['24','24','24','22']" data-width="['448','356','370','317']" data-height="['none','none','81','88']" data-whitespace="normal" data-transform_idle="o:1;" data-transform_in="x:-50px;opacity:0;s:1000;e:Power2.easeOut;" data-transform_out="opacity:0;s:1500;e:Power4.easeIn;s:1500;e:Power4.easeIn;" data-start="1500" data-splitin="none" data-splitout="none" data-responsive_offset="on" style="z-index: 15; min-width: 448px; max-width: 448px; white-space: normal;">Save Up To 30% On Cloud Servers
                        <br/> very faster server for your clients!
                    </div>
                    <div class="tp-caption rev-btn rev-btn  rs-parallaxlevel-8" id="slide-699-layer-8" data-x="['left','left','left','left']" data-hoffset="['30','30','200','80']" data-y="['middle','middle','top','top']" data-voffset="['228','228','456','400']" data-width="none" data-height="none" data-whitespace="nowrap" data-transform_idle="o:1;" data-transform_hover="o:1;rX:0;rY:0;rZ:0;z:0;s:300;e:Linear.easeNone;" data-style_hover="c:rgba(51, 51, 51, 1.00);bg:rgba(255, 255, 255, 1.00);" data-transform_in="x:-50px;opacity:0;s:1000;e:Power2.easeOut;" data-transform_out="opacity:0;s:1500;e:Power4.easeIn;s:1500;e:Power4.easeIn;" data-start="1750" data-splitin="none" data-splitout="none" data-actions='[{"event":"click","action":"scrollbelow","offset":"px"}]' data-responsive_offset="on" data-responsive="off" style="z-index: 16; white-space: nowrap; font-size: 16px; line-height: 48px; font-weight: 600; color: rgba(255, 255, 255, 1.00);font-family:Raleway;background-color:rgba(51, 51, 51, 1.00);padding:0px 40px 0px 40px;border-color:rgba(0, 0, 0, 1.00);border-width:2px;letter-spacing:1px;">GET STARTED TODAY
                    </div>
                </li>
            </ul>
            <div class="tp-static-layers">
                <!-- LAYER NR. 1 -->
                <div class="tp-caption -   tp-static-layer" id="slide-102-layer-1" data-x="['right','right','right','right']" data-hoffset="['30','30','30','30']" data-y="['top','top','top','top']" data-voffset="['30','30','30','30']" data-width="none" data-height="none" data-whitespace="nowrap" data-transform_idle="o:1;" data-transform_in="opacity:0;s:1000;e:Power3.easeInOut;" data-transform_out="auto:auto;s:1000;" data-start="500" data-splitin="none" data-splitout="none" data-actions='[{"event":"click","action":"toggleclass","layer":"slide-102-layer-1","delay":"0","classname":"open"},{"event":"click","action":"togglelayer","layerstatus":"hidden","layer":"slide-102-layer-3","delay":"0"},{"event":"click","action":"togglelayer","layerstatus":"hidden","layer":"slide-102-layer-4","delay":"0"},{"event":"click","action":"togglelayer","layerstatus":"hidden","layer":"slide-102-layer-5","delay":"0"},{"event":"click","action":"togglelayer","layerstatus":"hidden","layer":"slide-102-layer-6","delay":"0"}]' data-basealign="slide" data-responsive_offset="off" data-responsive="off" data-startslide="-1" data-endslide="-1" style="z-index: 5; white-space: nowrap; font-size: 20px; line-height: 22px; font-weight: 400; color: rgba(255, 255, 255, 1.00);">
                    <div id="rev-burger">
                        <span></span>
                        <span></span>
                        <span></span>
                    </div>
                </div>
            </div>
            <div class="tp-bannertimer tp-bottom" style="visibility: hidden !important;"></div>
        </div>
    </div>
    <!-- END REVOLUTION SLIDER -->
</div>

<section class="section nopadtop lb">
    <div class="container-fluid">
        <div class="row center-tab">
            <div class="panel with-nav-tabs panel-primary">
                <div class="panel-heading">
                    <ul class="nav nav-tabs">
                        <li class="active"><a href="#buydomain" data-toggle="tab">Buy a Domain</a></li>
                        <li><a href="#emailservice" data-toggle="tab">Buy Email Service</a></li>
                        <li><a href="#wordpresshost" data-toggle="tab">WordPress Hosting</a></li>
                    </ul>
                </div>
                <div class="panel-body container">
                    <div class="tab-content">
                        <div class="tab-pane fade in active" id="buydomain">
                            <div class="row">
                                <div class="col-md-8 col-sm-12 col-xs-12">
                                    <div class="wb">
                                        <div class="ribbon-wrapper-green"><div class="ribbon-green">10% Off</div></div>
                                        <div class="big-title">
                                            <h3>Looking a premium quality<br>
                                                <span>Domain Name?</span>
                                            </h3>
                                        </div><!-- end big-title -->

                                        <form class="checkdomain form-inline">
                                            <div class="checkdomain-wrapper">
                                                <p>With our awesome domain name search form, you can search any <strong>domain names</strong> with tons of extensions, for example .com .net .org and more.</p>
                                                <div class="form-group">
                                                    <label class="sr-only" for="domainnamehere">Domain name</label>
                                                    <input type="text" class="form-control" id="domainnamehere" placeholder="Enter domain name here..">
                                                    <button type="submit" class="btn btn-default"><i class="fa fa-search"></i> Search</button>
                                                </div>
                                                <hr>
                                                <div class="clearfix"></div>
                                                <div class="checkbox checkbox-warning">
                                                    <input id="domaincom" type="checkbox" class="styled" checked>
                                                    <label for="domaincom">.com</label>
                                                </div>
                                                <div class="checkbox checkbox-warning">
                                                    <input id="domainnet" type="checkbox" class="styled" checked>
                                                    <label for="domainnet">.net</label>
                                                </div>
                                                <div class="checkbox checkbox-warning">
                                                    <input id="domainorg" type="checkbox" class="styled">
                                                    <label for="domainorg">.org</label>
                                                </div>
                                                <div class="checkbox checkbox-warning">
                                                    <input id="domaintv" type="checkbox" class="styled">
                                                    <label for="domaintv">.tv</label>
                                                </div>
                                                <div class="checkbox checkbox-warning">
                                                    <input id="domaininfo" type="checkbox" class="styled">
                                                    <label for="domaininfo">.info</label>
                                                </div>
                                                <div class="checkbox checkbox-warning">
                                                    <input id="domainco" type="checkbox" class="styled">
                                                    <label for="domainco">.co</label>
                                                </div>
                                                <div class="checkbox checkbox-warning">
                                                    <input id="domainus" type="checkbox" class="styled">
                                                    <label for="domainus">.us</label>
                                                </div>
                                                <div class="checkbox checkbox-warning">
                                                    <input id="domaincc" type="checkbox" class="styled">
                                                    <label for="domaincc">.cc</label>
                                                </div>
                                                <div class="checkbox checkbox-warning">
                                                    <input id="domainmoda" type="checkbox" class="styled">
                                                    <label for="domainmoda">.moda</label>
                                                </div>
                                                <div class="checkbox checkbox-warning">
                                                    <input id="domainsale" type="checkbox" class="styled">
                                                    <label for="domainsale">.sale</label>
                                                </div>
                                            </div><!-- end checkdomain-wrapper -->
                                        </form>
                                    </div>
                                </div><!-- end col -->

                                <div class="col-md-4 col-sm-12 col-xs-12">
                                    <div class="banner">
                                        <img src="upload/banner_01.jpg" alt="" class="img-responsive">
                                        <div class="banner-desc">
                                            <h3>Transfer your own<br>
                                                <span>Domain Name</span>
                                            </h3>
                                        </div>
                                        <div class="banner-button">
                                            <a href="#" class="btn btn-primary btn-sm">Transfer Now</a>
                                        </div>
                                    </div><!-- end banner -->
                                </div><!-- end col -->
                            </div><!-- end row -->
                        </div><!-- end tab-pane -->

                        <div class="tab-pane fade" id="emailservice">
                            <div class="row">
                                <div class="col-md-6 col-sm-12 col-xs-12">
                                    <div class="wb">
                                        <div class="big-title">
                                            <h3>Professional email structure<br>
                                                <span>Use Your Domain E-mail</span>
                                            </h3>
                                        </div><!-- end big-title -->

                                        <div class="email-widget">
                                            <p>All IP addresses are used in the system are not included in the blacklist clean ip address system, so that no spam will not fall on your system.</p>
                                            <ul class="check-list">
                                                <li>POP/IMAP/Webmail Supported</li>
                                                <li>Daily 250 Email Sent Limit</li>
                                                <li>Anti-spam Protection</li>
                                                <li>All Mobile Devices Supported</li>
                                                <li>All Mobile Devices Supported</li>
                                            </ul><!-- end check -->
                                        </div><!-- end email widget -->
                                    </div><!-- end wb -->
                                </div><!-- end col -->

                                <div class="col-md-6 col-sm-12 col-xs-12">
                                    <div class="banner">
                                        <img src="upload/banner_02.jpg" alt="" class="img-responsive">
                                        <div class="banner-button">
                                            <a href="#" class="btn btn-primary">Buy Email Service</a>
                                        </div>
                                    </div><!-- end banner -->
                                </div><!-- end col -->
                            </div><!-- end row -->
                        </div><!-- end tab-pane -->

                        <div class="tab-pane fade" id="wordpresshost">
                            <div class="row">
                                <div class="col-md-4 col-sm-12 col-xs-12">
                                    <div class="wb">
                                        <div class="big-title">
                                            <h3>Best CM System<br>
                                                <span>WordPress</span>
                                            </h3>
                                        </div><!-- end big-title -->

                                        <div class="email-widget">
                                            <p>WordPress is WordPress is web software you can use to create a beautiful website and blog.</p>
                                            <ul class="check-list">
                                                <li>Buil Powerful Blogs</li>
                                                <li>5000+ Themes & Plugins</li>
                                                <li>10x Faster Sites</li>
                                                <li>Multi Site & Subdomain Support</li>
                                                <li>60+ Language Support</li>
                                            </ul><!-- end check -->
                                        </div><!-- end email widget -->
                                    </div><!-- end wb -->
                                </div><!-- end col -->

                                <div class="col-md-4 col-sm-12 col-xs-12">
                                    <div class="flat white-style">
                                        <ul class="plan plan1">
                                            <li class="plan-name">
                                                WordPress Package
                                            </li>
                                            <li class="plan-price">
                                                <strong>$11</strong> / month
                                            </li>
                                            <li>
                                                <strong>5GB</strong> Storage
                                            </li>
                                            <li>
                                                <strong>1GB</strong> RAM
                                            </li>
                                            <li>
                                                <strong>400GB</strong> Bandwidth
                                            </li>
                                            <li>
                                                <strong>10</strong> Email Address
                                            </li>
                                            <li>
                                                <strong>Forum</strong> Support
                                            </li>
                                            <li class="plan-action">
                                                <a href="#" class="btn btn-primary">Signup Now</a>
                                            </li>
                                        </ul>
                                    </div>
                                </div><!-- end col -->

                                <div class="col-md-4 col-sm-12 col-xs-12">
                                    <div class="wb">
                                        <div class="big-title">
                                            <h3>Powerful Package<br>
                                                <span>WordPress Host</span>
                                            </h3>
                                        </div><!-- end big-title -->

                                        <div class="email-widget">
                                            <p>We offer high-quality WordPress hosting plans for all WordPress users to get optimized!</p>
                                            <ul class="check-list">
                                                <li>Supercharged Speed</li>
                                                <li>WordPress Cache Plugins Support</li>
                                                <li>Awesome Security</li>
                                                <li>Superior Support</li>
                                                <li>Flexibility for Changing Demands</li>
                                            </ul><!-- end check -->
                                        </div><!-- end email widget -->
                                    </div><!-- end wb -->
                                </div><!-- end col -->
                            </div><!-- end row -->
                        </div><!-- end tab-pane -->
                    </div><!-- end tab-content -->
                </div><!-- end panel-body -->
            </div><!-- end panel -->
        </div><!-- end row -->
    </div><!-- end container -->
</section><!-- end section -->

<section class="section">
    <div class="container">
        <div class="section-title text-center">
            <h3>Package <span>Features</span></h3>
            <p class="last">
                If you are looking premium quality web hosting package for your website, you are right place!<br> We offer outstanding user friendly web hosting package for all!
            </p>
        </div><!-- end section-title -->

        <div class="row">
            <div class="col-md-3 col-sm-6 col-xs-12">
                <div class="custom-service colorful">
                    <img src="images/icons/white_icon_01.png" alt="" class="wow fadeIn">
                    <h4>Worldwide Connection</h4>
                    <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Quisque aliquam lectus eu purus imperdiet.</p>
                    <a href="#" class="btn btn-primary btn-sm">Read more</a>
                </div><!-- end custom-service -->
            </div><!-- end col -->

            <div class="col-md-3 col-sm-6 col-xs-12">
                <div class="custom-service colorful">
                    <img src="images/icons/white_icon_02.png" alt="" class="wow fadeIn">
                    <h4>24/7 Daily Backup</h4>
                    <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Quisque aliquam lectus eu purus imperdiet.</p>
                    <a href="#" class="btn btn-primary btn-sm">Read more</a>
                </div><!-- end custom-service -->
            </div><!-- end col -->

            <div class="col-md-3 col-sm-6 col-xs-12">
                <div class="custom-service colorful">
                    <img src="images/icons/white_icon_03.png" alt="" class="wow fadeIn">
                    <h4>Feed & High-Security</h4>
                    <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Quisque aliquam lectus eu purus imperdiet.</p>
                    <a href="#" class="btn btn-primary btn-sm">Read more</a>
                </div><!-- end custom-service -->
            </div><!-- end col -->

            <div class="col-md-3 col-sm-6 col-xs-12">
                <div class="custom-service colorful">
                    <img src="images/icons/white_icon_04.png" alt="" class="wow fadeIn">
                    <h4>10x Faster Features</h4>
                    <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Quisque aliquam lectus eu purus imperdiet.</p>
                    <a href="#" class="btn btn-primary btn-sm">Read more</a>
                </div><!-- end custom-service -->
            </div><!-- end col -->
        </div><!-- end row -->
    </div><!-- end container -->
</section><!-- end section -->

<section class="section searchbanner1 lightversion">
    <div class="container">
        <div class="section-title text-center">
            <h3>Company <span>Statics</span></h3>
            <p class="last">
                Let's see what other's say about HostHubs web hosting company!<br> 10.000+ customers can not be wrong!
            </p>
        </div><!-- end section-title -->
        <div class="row services-list hover-services text-center">
            <div class="col-md-3 col-sm-6 wow fadeIn">
                <div class="box">
                    <i class="fa fa-html5"></i>
                    <h3>Sites Hosted</h3>
                    <p class="stat-count">34551</p>
                </div><!-- end box -->
            </div>

            <div class="col-md-3 col-sm-6 wow fadeIn">
                <div class="box">
                    <i class="fa fa-smile-o"></i>
                    <h3>Happy Clients</h3>
                    <p class="stat-count">12560</p>
                </div><!-- end box -->
            </div>

            <div class="col-md-3 col-sm-6 wow fadeIn">
                <div class="box">
                    <i class="fa fa-envelope-o"></i>
                    <h3>Tickets Solved</h3>
                    <p class="stat-count">2214</p>
                </div><!-- end box -->
            </div>

            <div class="col-md-3 col-sm-6 wow fadeIn">
                <div class="box">
                    <i class="fa fa-at"></i>
                    <h3>Sold Domains</h3>
                    <p class="stat-count">16000</p>
                </div><!-- end box -->
            </div><!-- end col -->
        </div>
    </div><!-- end container -->
</section><!-- end section -->

<section class="section">
    <div class="container">
        <div class="row">
            <div class="col-md-6 col-sm-12">
                <div class="small-title">
                    <h3>Our Server Statics</h3>
                    <hr>
                </div><!-- end big-title -->

                <div class="skills custommargin text-left">
                    <div class="blue-color">
                        <h3>Up-Time Guarantee</h3>
                        <div class="progress">
                            <div class="progress-bar progress-bar-primary  progress-bar-striped active" role="progressbar" aria-valuenow="99" aria-valuemin="0" aria-valuemax="100" style="width: 99%">
                                <span class="sr-only">99% Complete (success)</span>
                            </div>
                        </div>
                    </div><!-- end green-color -->

                    <div class="blue-color">
                        <h3>CPU</h3>
                        <div class="progress">
                            <div class="progress-bar progress-bar-primary  progress-bar-striped active" role="progressbar" aria-valuenow="40" aria-valuemin="0" aria-valuemax="100" style="width: 90%">
                                <span class="sr-only">90% Complete (success)</span>
                            </div>
                        </div>
                    </div><!-- end green-color -->

                    <div class="blue-color">
                        <h3>RAM</h3>
                        <div class="progress">
                            <div class="progress-bar progress-bar-primary  progress-bar-striped active" role="progressbar" aria-valuenow="95" aria-valuemin="0" aria-valuemax="100" style="width: 95%">
                                <span class="sr-only">95%</span>
                            </div>
                        </div>
                    </div><!-- end green-color -->

                    <div class="blue-color">
                        <h3>SSD</h3>
                        <div class="progress">
                            <div class="progress-bar progress-bar-primary  progress-bar-striped active" role="progressbar" aria-valuenow="90" aria-valuemin="0" aria-valuemax="100" style="width: 90%">
                                <span class="sr-only">90%</span>
                            </div>
                        </div>
                    </div><!-- end green-color -->

                    <div class="blue-color">
                        <h3>Memory</h3>
                        <div class="progress">
                            <div class="progress-bar progress-bar-primary  progress-bar-striped active" role="progressbar" aria-valuenow="95" aria-valuemin="0" aria-valuemax="100" style="width: 95%">
                                <span class="sr-only">95%</span>
                            </div>
                        </div>
                    </div><!-- end green-color -->
                </div><!-- end skills -->
            </div><!-- end col -->

            <div class="col-md-6 col-sm-12 col-xs-12">
                <div class="small-title">
                    <h3>Pre-Sale Questions</h3>
                    <hr>
                </div><!-- end big-title -->

                <br>

                <div class="panel-group first-accordion withicon" id="accordion1">
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            <h4 class="panel-title">
                                <a data-toggle="collapse" data-parent="#accordion1" href="#collapse11"><i class="fa fa-question"></i> What's included?</a>
                            </h4>
                        </div>
                        <div id="collapse11" class="panel-collapse collapse">
                            <div class="panel-body">
                                <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.</p>
                            </div>
                        </div>
                    </div>
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            <h4 class="panel-title">
                                <a data-toggle="collapse" data-parent="#accordion1" href="#collapse22"><i class="fa fa-question"></i> What devices and browsers do you support?</a>
                            </h4>
                        </div>
                        <div id="collapse22" class="panel-collapse collapse">
                            <div class="panel-body">
                                <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.</p>
                            </div>
                        </div>
                    </div>
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            <h4 class="panel-title">
                                <a data-toggle="collapse" data-parent="#accordion1" href="#collapse33"><i class="fa fa-question"></i> Which license is right for me?</a>
                            </h4>
                        </div>
                        <div id="collapse33" class="panel-collapse collapse">
                            <div class="panel-body">
                                <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.</p>
                            </div>
                        </div>
                    </div>

                    <div class="panel panel-default">
                        <div class="panel-heading">
                            <h4 class="panel-title">
                                <a data-toggle="collapse" data-parent="#accordion1" href="#collapse44"><i class="fa fa-question"></i> What’s an example of a proper use for each license?</a>
                            </h4>
                        </div>
                        <div id="collapse44" class="panel-collapse collapse">
                            <div class="panel-body">
                                <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.</p>
                            </div>
                        </div>
                    </div>
                </div>
            </div><!-- end col -->
        </div><!-- end row -->
    </div><!-- end container -->
</section><!-- end section -->

<section class="smallsec">
    <div class="container">
        <div class="row">
            <div class="col-md-8 text-center">
                <h3>Start Building Your 10x Fast Website Today!</h3>
            </div>
            <div class="col-md-4 text-center">
                <a href="#" class="btn btn-primary btn-lg">Compose All Plans</a>
            </div><!-- end col -->
        </div><!-- end row -->
    </div><!-- end container -->
</section><!-- end section -->

<section class="section">
    <div class="container">
        <div class="section-title text-center">
            <h3>Plans & <span>Pricing</span></h3>
            <p class="last">
                Listed below our awesome hosting packages. You can select any web hosting services below!<br> If your package not listed here don't forget to check our <strong><a href="#">hosting compose</a></strong> page.
            </p>
        </div><!-- end section-title -->

        <div class="row pricing-bigger">
            <div class="col-md-4 col-sm-4 col-xs-12 pricing-choose nopadding pricing-border">
                <div class="pricing-header colorprimary">
                    <p>Professional Hosting Packages</p>
                    <h3>Compare plans</h3>
                </div>
                <div class="pricing-body">
                    <ul>
                        <li>Disk Space</li>
                        <li>Bandwidth</li>
                        <li>Free Domain</li>
                        <li>Free Dedicated IP</li>
                        <li>Free Private SSL</li>
                        <li>Free/Instant Setup</li>
                        <li>24/7/365 Support</li>
                        <li>99.9% Uptime Guarantee </li>
                        <li>90 Day Money Back Guarantee </li>
                        <li>Addon/Parked Domains </li>
                        <li>MySQL Databases</li>
                        <li>Streaming Audio/Video </li>
                        <li>&nbsp;</li>
                    </ul><!-- end ul -->
                </div><!-- end body -->
            </div><!-- end col -->

            <div class="col-md-2 col-sm-2 col-xs-12 pricing-plan nopadding text-center pricing-border">
                <div class="pricing-header color1">
                    <p>2€ per month</p>
                    <h3>Baby</h3>
                </div>
                <div class="pricing-body">
                    <ul>
                        <li>Unlimited</li>
                        <li>Unlimited</li>
                        <li>1</li>
                        <li><i class="fa fa-close"></i></li>
                        <li><i class="fa fa-close"></i></li>
                        <li><i class="fa fa-check"></i></li>
                        <li><i class="fa fa-check"></i></li>
                        <li><i class="fa fa-check"></i></li>
                        <li><i class="fa fa-check"></i></li>
                        <li><i class="fa fa-close"></i></li>
                        <li>Unlimited</li>
                        <li><i class="fa fa-close"></i></li>
                        <li><a href="#" class="btn btn-primary btn-block">Order Now</a></li>
                    </ul><!-- end ul -->
                </div><!-- end body -->
            </div><!-- end col -->

            <div class="col-md-2 col-sm-2 col-xs-12 pricing-plan nopadding text-center pricing-border">
                <div class="pricing-header color2">
                    <p>2€ per month</p>
                    <h3>Business</h3>
                </div>
                <div class="pricing-body">
                    <ul>
                        <li>Unlimited</li>
                        <li>Unlimited</li>
                        <li>3</li>
                        <li><i class="fa fa-close"></i></li>
                        <li><i class="fa fa-check"></i></li>
                        <li><i class="fa fa-check"></i></li>
                        <li><i class="fa fa-check"></i></li>
                        <li><i class="fa fa-check"></i></li>
                        <li><i class="fa fa-check"></i></li>
                        <li><i class="fa fa-close"></i></li>
                        <li>Unlimited</li>
                        <li><i class="fa fa-close"></i></li>
                        <li><a href="#" class="btn btn-primary btn-block">Order Now</a></li>
                    </ul><!-- end ul -->
                </div><!-- end body -->
            </div><!-- end col -->

            <div class="col-md-2 col-sm-2 col-xs-12 pricing-plan nopadding text-center pricing-border">
                <div class="pricing-header color3">
                    <p>2€ per month</p>
                    <h3>Ultimate</h3>
                </div>
                <div class="pricing-body">
                    <ul>
                        <li>Unlimited</li>
                        <li>Unlimited</li>
                        <li>5</li>
                        <li><i class="fa fa-close"></i></li>
                        <li><i class="fa fa-check"></i></li>
                        <li><i class="fa fa-check"></i></li>
                        <li><i class="fa fa-check"></i></li>
                        <li><i class="fa fa-check"></i></li>
                        <li><i class="fa fa-check"></i></li>
                        <li><i class="fa fa-check"></i></li>
                        <li>Unlimited</li>
                        <li><i class="fa fa-check"></i></li>
                        <li><a href="#" class="btn btn-primary btn-block">Order Now</a></li>
                    </ul><!-- end ul -->
                </div><!-- end body -->
            </div><!-- end col -->

            <div class="col-md-2 col-sm-2 col-xs-12 pricing-plan nopadding text-center">
                <div class="pricing-header color4">
                    <p>2€ per month</p>
                    <h3>Enterprise</h3>
                </div>
                <div class="pricing-body">
                    <ul>
                        <li>Unlimited</li>
                        <li>Unlimited</li>
                        <li>Unlimited</li>
                        <li><i class="fa fa-check"></i></li>
                        <li><i class="fa fa-check"></i></li>
                        <li><i class="fa fa-check"></i></li>
                        <li><i class="fa fa-check"></i></li>
                        <li><i class="fa fa-check"></i></li>
                        <li><i class="fa fa-check"></i></li>
                        <li><i class="fa fa-check"></i></li>
                        <li>Unlimited</li>
                        <li><i class="fa fa-check"></i></li>
                        <li><a href="#" class="btn btn-primary btn-block">Order Now</a></li>
                    </ul><!-- end ul -->
                </div><!-- end body -->
            </div><!-- end col -->

        </div><!-- end row -->
    </div><!-- end container -->
</section><!-- end section -->

<section class="section nopadding lb clearfix">
    <div class="container">
        <div class="row">
            <div class="col-md-5 col-sm-12 customsection hidden-sm hidden-xs">
                <img src="upload/section_02.jpg" alt="images">
                <div class="text_container">
                    <span class="logo"></span>
                    <h4>Hosting<br>Awesomeness</h4>
                    <p>Welcome to the HostHubs, we make really beautiful and amazing stuff. This can be used to describe what you do, how you do it, & who you do it for. Don’t miss today!</p>
                    <a href="#" class="btn btn-primary">Buy Package Now!</a>
                </div>
            </div>

            <div class="col-md-7 col-sm-12">
                <div class="box-feature-full clearfix">
                    <div class="vertical-elements">
                        <div class="box">
                            <div class="icon-container alignleft">
                                <img src="images/icons/features_box_01.png" alt="" class="img-responsive">
                            </div>

                            <div class="feature-desc">
                                <h4>Web Hosting</h4>
                                <p>Suspendisse posuere lectus sed nunc bibendum, in suscipit felis imperdiet.</p>
                            </div><!-- end desc -->
                        </div><!-- end featurebox -->

                        <hr>

                        <div class="box">
                            <div class="icon-container alignleft">
                                <img src="images/icons/features_box_02.png" alt="" class="img-responsive">
                            </div>

                            <div class="feature-desc">
                                <h4>Reseller Package</h4>
                                <p>Suspendisse posuere lectus sed nunc bibendum, in suscipit felis imperdiet.</p>
                            </div><!-- end desc -->
                        </div><!-- end featurebox -->

                        <hr>

                        <div class="box">
                            <div class="icon-container alignleft">
                                <img src="images/icons/features_box_03.png" alt="" class="img-responsive">
                            </div>

                            <div class="feature-desc">
                                <h4>Cloud Server</h4>
                                <p>Suspendisse posuere lectus sed nunc bibendum, in suscipit felis imperdiet.</p>
                            </div><!-- end desc -->
                        </div><!-- end featurebox -->
                    </div><!-- end vertical -->
                </div>
            </div>
        </div>
    </div>
</section><!-- end section -->

<section class="section">
    <div class="container">
        <div class="section-title text-center">
            <h3>Security <span>Features</span></h3>
            <p class="last">
                If you are looking premium quality web hosting package for your website, you are right place!<br> We offer outstanding user friendly web hosting package for all!
            </p>
        </div><!-- end section-title -->

        <div class="row">
            <div class="col-md-4 col-sm-6 col-xs-12">
                <div class="custom-service">
                    <img src="images/icons/custom_icon_01.png" alt="" class="wow fadeIn">
                    <h4>Pasword Security</h4>
                    <p>Duis at enim sed metus congue molestie. Mauris sed metus eu turpis sollicitudin pharetra in id nisi. Sed a libero at mauris ele.</p>
                </div><!-- end custom-service -->
            </div><!-- end col -->

            <div class="col-md-4 col-sm-6 col-xs-12">
                <div class="custom-service">
                    <img src="images/icons/custom_icon_02.png" alt="" class="wow fadeIn">
                    <h4>Mobile Optimized</h4>
                    <p>Vestibulum scelerisque nisl non sceleris metus eu turpique aliquam. Duis tristique massa eu posuere venenatis  metus eu turpi.</p>
                </div><!-- end custom-service -->
            </div><!-- end col -->

            <div class="col-md-4 col-sm-6 col-xs-12">
                <div class="custom-service">
                    <img src="images/icons/custom_icon_03.png" alt="" class="wow fadeIn">
                    <h4>Documentation Server</h4>
                    <p>Nulla sodales purus nec urna posuere malesuada. Vivamus finibus nunc non velit molestie, nec us nec urna posuere malesuada egestas.</p>
                </div><!-- end custom-service -->
            </div><!-- end col -->
        </div>

        <hr class="invis1">

        <div class="row">
            <div class="col-md-4 col-sm-6 col-xs-12">
                <div class="custom-service">
                    <img src="images/icons/custom_icon_04.png" alt="" class="wow fadeIn">
                    <h4>Worldwide Security</h4>
                    <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Quisque aliquam lectus eu purus imperdiet, dictum vulputate quam facilisis.</p>
                </div><!-- end custom-service -->
            </div><!-- end col -->

            <div class="col-md-4 col-sm-6 col-xs-12">
                <div class="custom-service">
                    <img src="images/icons/custom_icon_05.png" alt="" class="wow fadeIn">
                    <h4>Email Connection Security</h4>
                    <p>Curabitur sed diam vel ligula porttitor semper. Vivamus finibus nunc non velit molestie, nec egestas libero vestibulum.</p>
                </div><!-- end custom-service -->
            </div><!-- end col -->

            <div class="col-md-4 col-sm-6 col-xs-12">
                <div class="custom-service">
                    <img src="images/icons/custom_icon_06.png" alt="" class="wow fadeIn">
                    <h4>10x Security Features</h4>
                    <p>Phasellus fringilla metus non velit sagittis aliquam. Quisque aliquam lectus eu purus imperdiet, dictum vulputate quam facilisis.</p>
                </div><!-- end custom-service -->
            </div><!-- end col -->
        </div><!-- end row -->
    </div><!-- end container -->
</section><!-- end section -->

<section class="section searchbanner">
    <div class="container">
        <div class="section-title text-center">
            <h3><span>20,000+</span> SUBSCRIBERS CAN NOT BE WRONG</h3>
            <p class="last">
                Subscribe our newsletter for more discount coupon codes, daily deals of HostHubs goodies!
            </p>
        </div><!-- end section-title -->

        <div class="row">
            <div class="col-md-6 col-md-offset-3">
                <div class="widget clearfix text-center">
                    <form class="checkdomain form-inline">
                        <div class="checkdomain-wrapper">
                            <div class="form-group">
                                <label class="sr-only" for="domainnamehere-menu1">Domain name</label>
                                <input type="text" class="form-control" id="domainnamehere-menu1" placeholder="Enter your email here..">
                                <button type="submit" class="btn btn-default"><i class="fa fa-envelope-o"></i> Go</button>
                            </div>
                        </div><!-- end checkdomain-wrapper -->
                    </form>
                </div><!-- end widget -->
            </div>
        </div><!-- end row -->
    </div><!-- end container -->
</section><!-- end section -->

<section class="section nopadding">
    <div class="container-fluid">
        <div class="row text-center box-feature">
            <div class="col-md-6 col-sm-12 box-feature-04">
                <h4>Idea For Companies</h4>
                <h3>Reseller <strong>Hosting</strong></h3>
                <img src="images/icons/features_box_01.png" alt="" class="wow fadeInUp">
                <p class="lead">Sed non mauris vitae erat consequat auctor eu in elit. Class aptent tasociosqu ad litora torquent peer inpet mauris in erat justo. Nullam ac urna eu felis..</p>
            </div>

            <div class="col-md-6 col-sm-12 box-feature-03">
                <h4>Idea For Beginners</h4>
                <h3>Web <strong>Hosting</strong></h3>
                <img src="images/icons/features_box_02.png" alt="" class="wow fadeInUp">
                <p class="lead">Sed non mauris vitae erat consequat auctor eu in elit. Class aptent tasociosqu ad litora torquent peer inpet mauris in erat justo. Nullam ac urna eu felis..</p>
            </div>
        </div><!-- end row -->
    </div>
</section><!-- end section -->

<section class="section testibanner">
    <div class="container-fluid">
        <div class="section-title text-center">
            <h3>Happy <span>Testimonials</span></h3>
            <p class="last">
                Let's see what other's say about HostHubs web hosting company!<br> 10.000+ customers can not be wrong!
            </p>
        </div><!-- end section-title -->

        <div class="row">
            <div class="col-md-10 col-md-offset-1">
                <div class="row testimonials">
                    <div class="col-md-3 col-sm-6 m30">
                        <blockquote>
                            <p class="clients-words"> One of the best theme i ever found , i was always wishing theme like this for my project. i really love it good job!</p>
                            <span class="clients-name text-primary">— Amanda Corey</span>
                            <img class="img-circle img-thumbnail" src="upload/client_01.png" alt="">
                        </blockquote>
                    </div>
                    <div class="col-md-3 col-sm-6 m30">
                        <blockquote>
                            <p class="clients-words">HostHubs is an one powerful hosting company! Now with Php contact form. Perfect! Nice design, great support. </p>
                            <span class="clients-name text-primary">— Factory nn (@facnnteam)</span>
                            <img class="img-circle img-thumbnail" src="upload/client_02.png" alt="">
                        </blockquote>
                    </div>
                    <div class="col-md-3 col-sm-6">
                        <blockquote>
                            <p  class="clients-words"> It's an amazing hosting!. HostHubs is very easy to edit for a beginner and the design quality excellent.</p>
                            <span class="clients-name text-primary">— Martin Lorrenso</span>
                            <img class="img-circle img-thumbnail" src="upload/client_03.png" alt="">
                        </blockquote>
                    </div>
                    <div class="col-md-3 col-sm-6">
                        <blockquote>
                            <p  class="clients-words">Well designed and organized hosting package! with good amount of features available in it. High recommended!</p>
                            <span class="clients-name text-primary">— Boby Anderson</span>
                            <img class="img-circle img-thumbnail" src="upload/client_04.png" alt="">
                        </blockquote>
                    </div>
                </div>
            </div><!--/.col-->
        </div><!--/.row-->
    </div><!-- end container -->
</section><!-- end section -->

<section class="section overflow">
    <div class="container">
        <div class="row">
            <div class="col-md-8">
                <div class="big-title">
                    <h3>Installation Services<br>
                        <span>One Click Installation CMSPRO!</span>
                    </h3>
                </div><!-- end big-title -->

                <div class="hfeatures">
                    <p>If you are looking premium quality installation service for your next web design projects, we are professional team of content management systems! We build responsive websites with custom page builders!</p>

                    <ul class="work-elements">
                        <li>
                            <div class="box GrayScale">
                                <div class="icon-container">
                                    <img src="upload/work_01.png" alt="" class="img-responsive">
                                </div>
                            </div><!-- end featurebox -->
                        </li><!-- end col -->

                        <li>
                            <div class="box GrayScale">
                                <div class="icon-container">
                                    <img src="upload/work_02.png" alt="" class="img-responsive">
                                </div>
                            </div><!-- end featurebox -->
                        </li><!-- end col -->

                        <li>
                            <div class="box GrayScale">
                                <div class="icon-container">
                                    <img src="upload/work_03.png" alt="" class="img-responsive">
                                </div>
                            </div><!-- end featurebox -->
                        </li><!-- end col -->

                        <li>
                            <div class="box GrayScale">
                                <div class="icon-container">
                                    <img src="upload/work_04.png" alt="" class="img-responsive">
                                </div>
                            </div><!-- end featurebox -->
                        </li><!-- end col -->

                        <li>
                            <div class="box GrayScale">
                                <div class="icon-container">
                                    <img src="upload/work_05.png" alt="" class="img-responsive">
                                </div>
                            </div><!-- end featurebox -->
                        </li><!-- end col -->

                        <li>
                            <div class="box GrayScale">
                                <div class="icon-container">
                                    <img src="upload/work_06.png" alt="" class="img-responsive">
                                </div>
                            </div><!-- end featurebox -->
                        </li><!-- end col -->
                    </ul>
                </div><!-- end hfeatures -->
            </div><!-- end col -->
        </div><!-- end row -->
    </div><!-- end container -->

    <div class="image-box hidden-sm hidden-xs wow slideInRight">
        <img alt="" src="upload/device_07.png">
    </div>
</section><!-- end section -->

<section class="smallsec">
    <div class="container">
        <div class="row">
            <div class="col-md-8 text-center">
                <h3>Have a pre-sale question for our packages?</h3>
            </div>
            <div class="col-md-4 text-center">
                <a href="#" class="btn btn-primary btn-lg"><i class="fa fa-comments-o"></i> Online Chat</a>
            </div><!-- end col -->
        </div><!-- end row -->
    </div><!-- end container -->
</section><!-- end section -->