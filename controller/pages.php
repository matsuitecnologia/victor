<?php
/**
 * Pages
 *
 * Website content
 * @package 
 * @name Content
 * @author Angelo <contato@matsuitecnologia.com.br>
 * @version 0.1
 */
//namespace content;

class Pages
{
    public function friedlyUrl($urlAdress)
    {
    
        $keyCode = '/(http|www|ftp|.dat|.txt|.gif|wget)/';

        if (preg_match($keyCode, $urlAdress)) {
            echo "Erro na URL!";
        //Se a variável passada estiver dentro das normas, executa o else abaixo
        } else {
            if( !empty($urlAdress) ) {
                $url = explode( "/" , $urlAdress);
                if( empty($url[count($url)-1]) ) {
                    unset($url[count($url)-1]);
                }
            } else {
                $url[0] = "";
            }
        }

        require 'config/config.php';

        try {
            $sql = "SELECT * FROM 
                        ts_pages 
                    WHERE
                        page_url = ? AND
                        page_status = ?;";
            $sth = $dbh->prepare($sql);
            $sth->bindParam(1, $urlAdress, PDO::PARAM_STR);
            $sth->bindValue(2, 'publish', PDO::PARAM_STR);
            $sth->execute();

            while ( $row = $sth->fetch( PDO::FETCH_ASSOC ) ){

                if ("" == $row['page_url']) {
                    $pageUrl = "view/inicio.php";
                } else {
                    $pageUrl = "view/".$row['page_url'].".php";
                }

                $pageParam = array(
                    'pageId'          => $row['page_id'],
                    'pageAuthor'      => $row['page_author'],
                    'pageDate'        => $row['page_date'],
                    'pageContent'     => $row['page_content'],
                    'pageTitle'       => $row['page_title'],
                    'pageUrl'         => $row['page_url'],
                    'pageDescription' => $row['page_description'],
                    'pageKeywords'    => $row['page_keywords'],
                    'page'            => $pageUrl
                );

                return $pageParam;

            }

        } catch (PDOException $e) {
            print "Error!:" . $e->getMessage() . "<br/>";
            die();
        }
    }   
}