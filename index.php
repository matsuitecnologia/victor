<?php  
ini_set('display_errors',0);
ini_set('display_startup_erros',0);
error_reporting(E_ALL);

require_once('config/config.php');
require 'controller/pages.php';

$pages = new Pages();

$url = empty($_GET['url']) ? "" : $_GET['url'];


$conteudo = $pages->friedlyUrl($url);



date_default_timezone_set('America/Sao_Paulo');

?>


<head>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">

<!-- SITE META -->
<title>Matsui Tecnologia</title>
<meta name="description" content="">
<meta name="author" content="">
<meta name="keywords" content="">

<!-- FAVICONS -->
<link rel="shortcut icon" href="public/images/favicon.ico" type="image/x-icon">
<link rel="apple-touch-icon" href="public/images/apple-touch-icon.png">
<!--
<link rel="apple-touch-icon" sizes="57x57" href="public/images/apple-touch-icon-57x57.png">
<link rel="apple-touch-icon" sizes="72x72" href="public/images/apple-touch-icon-72x72.png">
<link rel="apple-touch-icon" sizes="76x76" href="public/images/apple-touch-icon-76x76.png">
<link rel="apple-touch-icon" sizes="114x114" href="public/images/apple-touch-icon-114x114.png">
<link rel="apple-touch-icon" sizes="120x120" href="public/images/apple-touch-icon-120x120.png">
<link rel="apple-touch-icon" sizes="144x144" href="public/images/apple-touch-icon-144x144.png">
<link rel="apple-touch-icon" sizes="152x152" href="public/images/apple-touch-icon-152x152.png">
--->
<!-- REVOLUTION STYLE SHEETS -->
<link rel="stylesheet" type="text/css" href="public/revolution/css/settings.css">
<!-- REVOLUTION LAYERS STYLES -->
<link rel="stylesheet" type="text/css" href="public/revolution/css/layers.css">
<!-- REVOLUTION NAVIGATION STYLES -->
<link rel="stylesheet" type="text/css" href="public/revolution/css/navigation.css">

<!-- BOOTSTRAP STYLES -->
<link rel="stylesheet" type="text/css" href="public/css/bootstrap.css">
<!-- TEMPLATE STYLES -->
<link rel="stylesheet" type="text/css" href="public/css/style.css">
<!-- RESPONSIVE STYLES -->
<link rel="stylesheet" type="text/css" href="public/css/responsive.css">
<!-- CUSTOM STYLES -->
<link rel="stylesheet" type="text/css" href="public/css/custom.css">




<!--[if IE]>
<script src="http://html5shiv.googlecode.com/svn/trunk/html5.js"></script>
<![endif]-->



</head>

<body>

<!-- PRELOADER -->
<div id="loader">
<div class="loader-container">
<img src="public/images/load.gif" alt="" class="loader-site spinner">
</div>
</div>
<!-- END PRELOADER -->

<div id="wrapper">
<div class="topbar">
<div class="container">
<div class="row">
<div class="col-md-5 col-sm-12 text-left cenmobile">
<div class="topmenu">
<span style="font-size: large" ><i class=" fa fa-whatsapp"></i> <a href="#"> (13) 98204-2274</a></span>
<span style="font-size: large"><i class="fa fa-envelope-o"></i> <a href="mailto:#">Contato@matsuitecnologia.com</a></span>




</div><!-- end callus -->
</div>

<div class="col-md-6 col-sm-12 text-right cenmobile">
<div class="topmenu rightv">
<div class="dropdown">

</a>

</div>
<span><a data-placement="bottom" data-toggle="tooltip" title="Brasil" href="#"><img src="public/images/br.png" alt=""></a></span>
<span><a data-placement="bottom" data-toggle="tooltip" title="USA" href="#"><img src="public/images/usa.png" alt=""></a></span>
<span><a data-placement="bottom" data-toggle="tooltip" title="Espanha" href="#"><img src="public/images/esp.png" alt=""></a></span>
</div><!-- end callus -->
</div><!-- end col -->
</div><!-- end row -->
</div><!-- end topbar -->
</div><!-- end topbar -->

<header class="header">
<div class="container-fluid">
<nav class="navbar navbar-default yamm">
<div class="container">
<div class="navbar-header">
<button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">
<span class="sr-only">Toggle navigation</span>
<span class="icon-bar"></span>
<span class="icon-bar"></span>
<span class="icon-bar"></span>
</button>
<a class="navbar-brand" href="/site"><img src="public/images/logo.png" width="200" height="60" alt=""></a>
</div>

<div id="navbar" class="navbar-collapse collapse">
<ul class="nav navbar-nav">
<li class="dropdown has-submenu active">
<a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">Inicio<span class="fa fa-angle-down"></span></a>


<li class="megamenu dropdown hasmenu">
<a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">Produtos e serviços <span class="fa fa-angle-down"></span> <span class="label label-success hidden-xs">Novidades</span></a>
<ul class="dropdown-menu withbgcolor">
<li>
<div class="yamm-content clearfix">
<div class="row">
<div class="col-md-4 col-sm-12 col-xs-12">
<div class="widget clearfix">
<div class="big-title">
<h3>
<img src="public/images/hostmaster.jpg" width="280" height="140">
</h3>
</div><!-- end big-title -->


</div>
</div><!-- end col -->
<div class="col-md-3 col-sm-12 col-xs-12">
<div class="widget clearfix">
<div class="widget-title">
<h4>Hospedagem</h4>
</div><!-- end widget-title -->

<div class="link-widget">
<ul>
<li><a href="planos">Plano de Hospedagem</a></li>
<li><a href="migre">Migre para Matsui</a></li>
<li><a href="email-personalizado">Email Personalizado</a></li>

</ul><!-- end check -->
</div><!-- end link-widget -->
</div>
</div>
<div class="col-md-3 col-sm-12 col-xs-12">
<div class="widget clearfix">
<div class="widget-title">
<h4>Divulgue seu site </h4>
</div><!-- end widget-title -->

<div class="link-widget">
<ul>
<li><a href="email-mark">E-mail Marketing</a></li>
<li><a href="criador-de-site">Criador de site </a></li>




</ul><!-- end check -->
</div><!-- end link-widget -->
</div>
</div>
<div class="col-md-2 col-sm-12 col-xs-12">
<div class="widget clearfix">
<div class="widget-title">
<h4>Adicionais</h4>
</div><!-- end widget-title -->

<div class="link-widget">
<ul>
<li><a href="anti-spam">Antispam</a></li>
<li><a href="ssl">Certificado SSl</a></li>
<li><a href="instalador-de-aplicativo">instalador de aplicativos</a></li>


</ul><!-- end check -->
</div><!-- end link-widget -->
</div><!-- end widget -->
</div>
</div><!-- end row -->
</div>
</li>
</ul>
</li>

<!-- Menu de Designers-->


<li class="megamenu dropdown hasmenu">
<a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">Designers e projetos <span class="fa fa-angle-down"></span> <span class="label label-success hidden-xs">Novidades</span></a>
<ul class="dropdown-menu withbgcolor">
<li>
<div class="yamm-content clearfix">
<div class="row">
<div class="col-md-4 col-sm-12 col-xs-12">
<div class="widget clearfix">
<div class="big-title">
<h3>
<img src="public/images/projeto.jpg" width="280" height="140">
</h3>
</div><!-- end big-title -->


</div>
</div><!-- end col -->

<div class="col-md-3 col-sm-12 col-xs-12">
<div class="widget clearfix">
<div class="widget-title">
<h4>Desenvolvimento Web</h4>
</div><!-- end widget-title -->

<div class="link-widget">
<ul>
<li><a href="planos">Site Institucional</a></li>
<li><a href="migre">Loja Virtual | E-Commerce </a></li>
<li><a href="email-personalizado">Projetos Personalizados</a></li>

</ul><!-- end check -->
</div><!-- end link-widget -->
</div>
</div>






<div class="col-md-3 col-sm-12 col-xs-12">
<div class="widget clearfix">
<div class="widget-title">
<h4>Modelos Pronta Entrega</h4>
</div><!-- end widget-title -->

<div class="link-widget">
<ul>
<li><a href="email-mark">Sites Prontos</a></li>
<li><a href="criador-de-site">Sistemas Prontos </a></li>
<li><a href="criador-de-site">Lojas Prontas </a></li>



</ul><!-- end check -->
</div><!-- end link-widget -->
</div>
</div>








<div class="col-md-2 col-sm-12 col-xs-12">
<div class="widget clearfix">
<div class="widget-title">
<h4>Assinatura Mensal</h4>
</div><!-- end widget-title -->

<div class="link-widget">
<ul>
<li><a href="anti-spam">Site Institucional</a></li>
<li><a href="migre">Loja Virtual | E-Commerce </a></li>
<li><a href="instalador-de-aplicativo">Programas Empresárias </a></li>


</ul><!-- end check -->
</div><!-- end link-widget -->
</div><!-- end widget -->
</div>
</div><!-- end row -->
</div>
</li>
</ul>
</li>




<li class="dropdown has-submenu">
<a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">Sobre a Matsui <span class="fa fa-angle-down"></span></a>
<ul class="dropdown-menu">

<li><a href="sobre">Quem Somos</a></li>
<li><a href="#">Motivo para Confiar</a></li>

</ul>
</li>

<li class="dropdown has-submenu">
<a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">Precisa de ajuda? <span class="fa fa-angle-down"></span></a>
<ul class="dropdown-menu">

<li><a href="fale-conosco">Fale conosco</a></li>
<li><a href="#">Perguntas Frequentes</a></li>
<li><a href="#">Central de Ajuda</a></li>
</ul>
</li>

</ul>


<ul class="nav navbar-nav navbar-right hidden-xs">
<li class="dropdown searchmenu hasmenu">
<a href="painel-cliente" class="dropdown-toggle" role="button" target="_blank" aria-haspopup="true" aria-expanded="false"> Entre<i class="fa fa-angle-down"></i></a>

</div><!--/.nav-collapse -->
</div><!--/.container-fluid -->
</nav><!-- end nav -->
</div><!-- end container -->
</header><!-- end header -->

<div class="after-header">
<div class="container">
<div class="row">
<div class="col-md-8 col-md-offset-2">
<ul class="list-inline text-center">
<li><a href="#">Web Hosting</a></li>
<li><a href="#">Cloud Server</a></li>
<li><a href="#">E-Mail</a></li>

</ul>
</div>
</div><!-- end row -->
</div><!-- end container -->
</div><!-- end after-header -->


<?php include $conteudo ['page']; ?>







<footer class="footer lb">
<div class="container">
<div class="row">
<div class="col-md-3 col-sm-12">
<div class="widget clearfix">
<div class="widget-title">
<h4>Hospedagem</h4>
</div><!-- end widget-title -->

<div class="link-widget">
<ul class="check">
<li><a href="#">Planos de Site</a></li>
<li><a href="#">Migre para Matsui</a></li>
<li><a href="#">Email Personalizado</a></li>

</ul><!-- end check -->
</div><!-- end link-widget -->
</div>

<hr>

<div class="widget clearfix">
<div class="widget-title">
<h4>Divulgue seu site</h4>
</div><!-- end widget-title -->

<div class="link-widget">
<ul class="check">
<li><a href="#">E-mail Marketing</a></li>
<li><a href="#">Criador de Site</a></li>
</ul><!-- end check -->
</div><!-- end link-widget -->
</div>
</div><!-- end col -->

<div class="col-md-3 col-sm-12">
<div class="widget clearfix">
<div class="widget-title">
<h4>Adicionais</h4>
</div><!-- end widget-title -->

<div class="link-widget">
<ul class="check">
<li><a href="#">Antispam</a></li>
<li><a href="#">Certificado SSL</a></li>
<li><a href="#">Instalador de Aplicativos</a></li>

</ul><!-- end check -->
</div><!-- end link-widget -->
</div>
</div><!-- end col -->

<div class="col-md-3 col-sm-12">
<div class="widget clearfix">
<div class="widget-title">
<h4>Suporte</h4>
</div><!-- end widget-title -->

<div class="link-widget">
<ul class="check">
<li><a href="#">Fale Conosco</a></li>
<li><a href="#">Perguntas frequentes</a></li>
<li><a href="#">Central de Ajuda</a></li>
</ul><!-- end check -->
</div><!-- end link-widget -->
</div>

<hr>

<div class="widget clearfix">
<div class="widget-title">
<h4>Sobre a Matsui</h4>
</div><!-- end widget-title -->

<div class="link-widget">
<ul class="check">
<li><a href="#">Quem Somos</a></li>
<li><a href="#">Motivos para confiar</a></li>

</ul><!-- end check -->
</div><!-- end link-widget -->
</div>
</div><!-- end col -->

<div class="col-md-3 col-sm-12">
<div class="widget clearfix">
<div class="widget-title">
<h4>Receba nossa novidades</h4>
</div><!-- end widget-title -->

<div class="newsletter-widget">
<p>Assine a nossa newsletter e fiquei por dentro das novidades</p>
<form>
<input type="text" class="form-control input-lg" placeholder="Seu Nome" />
<input type="email" class="form-control input-lg" placeholder=" Seu Email" />
<button class="btn btn-primary btn-block">Assine aqui !</button>
</form>
</div><!-- end newsletter -->
</div>

<hr>

<div class="widget clearfix">
<div class="widget-title">

</div>
</div><!-- end col -->
</div><!-- end row -->
</div><!-- end container -->
</footer><!-- end footer -->

<div class="footer-distributed">
<div class="container">
<div class="row">
<div class="col-md-4 col-sm-12 footer-left">
<div class="widget">
<img src="public/images/logo-matsui-branco.png" width="166" height="52" alt="">

<p class="footer-company-name">Matsui tecnologia &copy; 2017 Todos os Direitos Reservados</p>
</div>
</div>

<div class="col-md-4 col-sm-12 footer-center">
<div class="widget">
<div>
<i class="fa fa-map-marker"></i>
<p>Rua cunhambebe - N° 190 </p>
</div>

<div>
<i class="fa fa-map-marker"></i>
<p>Cep: 11450-090 - Guarujá </p>
</div>

<div>
<i class="fa fa-phone"></i>
<p>+55 13 0000-0000</p>
</div>

<div>
    <i class="fa fa-whatsapp"></i>
    <p>+55 13 00000-0000</p>
</div>


<div>
<i class="fa fa-envelope-o"></i>
<p><a href="mailto:support@company.com">contatot@matsuitecnologia.com.br</a></p>
</div>
</div>
</div>

<div class="col-md-4 col-sm-12 footer-right">
<div class="widget">
<p class="footer-company-about">
<span>Siga-nos</span>

</p>
<div class="footer-icons">
<a href="#"><i class="fa fa-facebook"></i></a>
<a href="#"><i class="fa fa-twitter"></i></a>
<a href="#"><i class="fa fa-linkedin"></i></a>
<a href="#"><i class="fa fa-google"></i></a>
</div>
</div>
</div>
</div><!-- end row -->
</div><!-- end container -->
</div><!-- end copyrights -->



<div class="dmtop">Scroll to Top</div>
</div><!-- end wrapper -->










<!-- scripts-->

<!-- Main Scripts-->
<script src="public/js/jquery.js"></script>
<script src="public/js/bootstrap.min.js"></script>
<script src="public/js/plugins.js"></script>

<!-- REVOLUTION JS FILES -->
<script type="text/javascript" src="public/revolution/js/jquery.themepunch.tools.min.js"></script>
<script type="text/javascript" src="public/revolution/js/jquery.themepunch.revolution.min.js"></script>

<!-- SLIDER REVOLUTION 5.0 EXTENSIONS -->
<script type="text/javascript" src="public/revolution/js/extensions/revolution.extension.actions.min.js"></script>
<script type="text/javascript" src="public/revolution/js/extensions/revolution.extension.carousel.min.js"></script>
<script type="text/javascript" src="public/revolution/js/extensions/revolution.extension.kenburn.min.js"></script>
<script type="text/javascript" src="public/revolution/js/extensions/revolution.extension.layeranimation.min.js"></script>
<script type="text/javascript" src="public/revolution/js/extensions/revolution.extension.migration.min.js"></script>
<script type="text/javascript" src="public/revolution/js/extensions/revolution.extension.navigation.min.js"></script>
<script type="text/javascript" src="public/revolution/js/extensions/revolution.extension.parallax.min.js"></script>
<script type="text/javascript" src="public/revolution/js/extensions/revolution.extension.slideanims.min.js"></script>
<script type="text/javascript" src="public/revolution/js/extensions/revolution.extension.video.min.js"></script>
<script src="public/js/revslider_04.js"></script>




</body>

</html>

