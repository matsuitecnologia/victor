<?php  
ini_set('display_errors',0);
ini_set('display_startup_erros',0);
error_reporting(E_ALL);

require_once('../config/config.php');
require 'controller/Pages.php';

$pages = new Pages();

$url = empty($_GET['url']) ? "" : $_GET['url'];

$conteudo = $pages->friedlyUrl($url);

date_default_timezone_set('America/Sao_Paulo');

if (!isset($_SESSION['userId'])) {
//if ($_SESSION['userId'] == "") {
	header("Location: login.php");
}

?>






<!DOCTYPE html>
<!--[if IE 9]>         <html class="no-js lt-ie10" lang="en"> <![endif]-->
<!--[if gt IE 9]><!--> <html class="no-js" lang="en"> <!--<![endif]-->
<head>
<meta charset="utf-8">

<title>Painel Me Sports</title>

<meta name="description" content="Me sports .">
<meta name="author" content="pixelcave">
<meta name="robots" content="noindex, nofollow">
<meta name="viewport" content="width=device-width,initial-scale=1.0,user-scalable=0">

<!-- Icons -->
<!-- The following icons can be replaced with your own, they are used by desktop and mobile browsers -->
<link rel="shortcut icon" href="public/img/favicon.png">
<link rel="apple-touch-icon" href="public/img/favicon.png" sizes="57x57">
<link rel="apple-touch-icon" href="public/img/favicon.png" sizes="72x72">
<link rel="apple-touch-icon" href="public/img/favicon.png" sizes="76x76">
<link rel="apple-touch-icon" href="public/img/favicon.png" sizes="114x114">
<link rel="apple-touch-icon" href="public/img/favicon.png" sizes="120x120">
<link rel="apple-touch-icon" href="public/img/favicon.png" sizes="144x144">
<link rel="apple-touch-icon" href="public/img/favicon.png" sizes="152x152">
<link rel="apple-touch-icon" href="public/img/favicon.png" sizes="180x180">
<!-- END Icons -->

<!-- Stylesheets -->
<!-- Bootstrap is included in its original form, unaltered -->
<link rel="stylesheet" href="public/css/bootstrap.min.css">

<!-- Related styles of various icon packs and plugins -->
<link rel="stylesheet" href="public/css/plugins.css">

<!-- The main stylesheet of this template. All Bootstrap overwrites are defined in here -->
<link rel="stylesheet" href="public/css/main.css">

<!-- Include a specific file here from public/css/themes/ folder to alter the default theme of the template -->

<!-- The themes stylesheet of this template (for using specific theme color in individual elements - must included last) -->
<link rel="stylesheet" href="public/css/themes.css">
<!-- END Stylesheets -->

<!-- Modernizr (browser feature detection library) -->
<script src="public/public/js/vendor/modernizr.min.js"></script>


<!-- jQuery, Bootstrap.js, jQuery plugins and Custom JS code -->
<script src="public/js/vendor/jquery.min.js"></script>
<script src="public/js/vendor/bootstrap.min.js"></script>
<script src="public/js/plugins.js"></script>
<script src="public/js/app.js"></script>

<!-- Google Maps API Key (you will have to obtain a Google Maps API key to use Google Maps) -->
<!-- For more info please have a look at https://developers.google.com/maps/documentation/javascript/get-api-key#key -->
<script src="https://maps.googleapis.com/maps/api/js?key="></script>
<script src="public/js/helpers/gmaps.min.js"></script>

<!-- Load and execute javascript code used only in this page -->
<script src="public/js/pages/index.js"></script>

<script src="public/js/vendor/modernizr.min.js"></script>


<script src="public/js/pages/compCalendar.js"></script>


<script type="text/javascript" src="public/js/custom.js" ></script>

<!-- data table -->

<script src="public/js/pages/tablesDatatables.js"></script>
<script>$(function(){ TablesDatatables.init(); });</script>



</head>
<body>
<!-- Page Wrapper -->
<!-- In the PHP version you can set the following options from inc/config file -->
<!--
Available classes:

'page-loading'      enables page preloader
-->
<div id="page-wrapper">
<!-- Preloader -->
<!-- Preloader functionality (initialized in public/public/js/app.js) - pageLoading() -->
<!-- Used only if page preloader is enabled from inc/config (PHP version) or the class 'page-loading' is added in #page-wrapper element (HTML version) -->
<div class="preloader themed-background">
<h1 class="push-top-bottom text-light text-center"><strong>Matsui</strong>Tecnologia</h1>
<div class="inner">
<h3 class="text-light visible-lt-ie10"><strong>Loading..</strong></h3>
<div class="preloader-spinner hidden-lt-ie10"></div>
</div>
</div>
<!-- END Preloader -->


<div id="page-container" class="sidebar-partial sidebar-visible-lg sidebar-no-animations">
<!-- Alternative Sidebar -->
<div id="sidebar-alt">
<!-- Wrapper for scrolling functionality -->
<div id="sidebar-alt-scroll">
<!-- Sidebar Content -->
<div class="sidebar-content">
<!-- Chat -->
<!-- Chat demo functionality initialized in public/public/js/app.js -> chatUi() -->
<a href="page_ready_chat.html" class="sidebar-title">
<i class="gi gi-comments pull-right"></i> <strong>Chat</strong>UI
</a>
<!-- Chat Users -->
<ul class="chat-users clearfix">
<li>
<a href="javascript:void(0)" class="chat-user-online">
<span></span>
<img src="public/img/placeholders/avatars/avatar12.jpg" alt="avatar" class="img-circle">
</a>
</li>
<li>
<a href="javascript:void(0)" class="chat-user-online">
<span></span>
<img src="public/img/placeholders/avatars/avatar15.jpg" alt="avatar" class="img-circle">
</a>
</li>
<li>
<a href="javascript:void(0)" class="chat-user-online">
<span></span>
<img src="public/img/placeholders/avatars/avatar10.jpg" alt="avatar" class="img-circle">
</a>
</li>
<li>
<a href="javascript:void(0)" class="chat-user-online">
<span></span>
<img src="public/img/placeholders/avatars/avatar4.jpg" alt="avatar" class="img-circle">
</a>
</li>
<li>
<a href="javascript:void(0)" class="chat-user-away">
<span></span>
<img src="public/img/placeholders/avatars/avatar7.jpg" alt="avatar" class="img-circle">
</a>
</li>
<li>
<a href="javascript:void(0)" class="chat-user-away">
<span></span>
<img src="public/img/placeholders/avatars/avatar9.jpg" alt="avatar" class="img-circle">
</a>
</li>
<li>
<a href="javascript:void(0)" class="chat-user-busy">
<span></span>
<img src="public/img/placeholders/avatars/avatar16.jpg" alt="avatar" class="img-circle">
</a>
</li>
<li>
<a href="javascript:void(0)">
<span></span>
<img src="public/img/placeholders/avatars/avatar1.jpg" alt="avatar" class="img-circle">
</a>
</li>
<li>
<a href="javascript:void(0)">
<span></span>
<img src="public/img/placeholders/avatars/avatar4.jpg" alt="avatar" class="img-circle">
</a>
</li>
<li>
<a href="javascript:void(0)">
<span></span>
<img src="public/img/placeholders/avatars/avatar3.jpg" alt="avatar" class="img-circle">
</a>
</li>
<li>
<a href="javascript:void(0)">
<span></span>
<img src="public/img/placeholders/avatars/avatar13.jpg" alt="avatar" class="img-circle">
</a>
</li>
<li>
<a href="javascript:void(0)">
<span></span>
<img src="public/img/placeholders/avatars/avatar5.jpg" alt="avatar" class="img-circle">
</a>
</li>
</ul>
<!-- END Chat Users -->

<!-- Chat Talk -->
<div class="chat-talk display-none">
<!-- Chat Info -->
<div class="chat-talk-info sidebar-section">
<button id="chat-talk-close-btn" class="btn btn-xs btn-default pull-right">
<i class="fa fa-times"></i>
</button>
<img src="public/img/placeholders/avatars/avatar5.jpg" alt="avatar" class="img-circle pull-left">
<strong>John</strong> Doe
</div>
<!-- END Chat Info -->

<!-- Chat Messages -->
<ul class="chat-talk-messages">
<li class="text-center"><small>Yesterday, 18:35</small></li>
<li class="chat-talk-msg animation-slideRight">Hey admin?</li>
<li class="chat-talk-msg animation-slideRight">How are you?</li>
<li class="text-center"><small>Today, 7:10</small></li>
<li class="chat-talk-msg chat-talk-msg-highlight themed-border animation-slideLeft">I'm fine, thanks!</li>
</ul>
<!-- END Chat Messages -->

<!-- Chat Input -->
<form action="index.html" method="post" id="sidebar-chat-form" class="chat-form">
<input type="text" id="sidebar-chat-message" name="sidebar-chat-message" class="form-control form-control-borderless" placeholder="Type a message..">
</form>
<!-- END Chat Input -->
</div>
<!--  END Chat Talk -->
<!-- END Chat -->

<!-- Activity -->
<a href="javascript:void(0)" class="sidebar-title">
<i class="fa fa-globe pull-right"></i> <strong>Activity</strong>UI
</a>
<div class="sidebar-section">
<div class="alert alert-danger alert-alt">
<small>just now</small><br>
<i class="fa fa-thumbs-up fa-fw"></i> Upgraded to Pro plan
</div>
<div class="alert alert-info alert-alt">
<small>2 hours ago</small><br>
<i class="gi gi-coins fa-fw"></i> You had a new sale!
</div>
<div class="alert alert-success alert-alt">
<small>3 hours ago</small><br>
<i class="fa fa-plus fa-fw"></i> <a href="page_ready_user_profile.html"><strong>John Doe</strong></a> would like to become friends!<br>
<a href="javascript:void(0)" class="btn btn-xs btn-primary"><i class="fa fa-check"></i> Accept</a>
<a href="javascript:void(0)" class="btn btn-xs btn-default"><i class="fa fa-times"></i> Ignore</a>
</div>
<div class="alert alert-warning alert-alt">
<small>2 days ago</small><br>
Running low on space<br><strong>18GB in use</strong> 2GB left<br>
<a href="page_ready_pricing_tables.html" class="btn btn-xs btn-primary"><i class="fa fa-arrow-up"></i> Upgrade Plan</a>
</div>
</div>
<!-- END Activity -->

<!-- Messages -->
<a href="page_ready_inbox.html" class="sidebar-title">
<i class="fa fa-envelope pull-right"></i> <strong>Messages</strong>UI (6)
</a>
<div class="sidebar-section">
<div class="alert alert-alt">
Debra Stanley<small class="pull-right">just now</small><br>
<a href="page_ready_inbox_message.html"><strong>New Follower</strong></a>
</div>
<div class="alert alert-alt">
Sarah Cole<small class="pull-right">2 min ago</small><br>
<a href="page_ready_inbox_message.html"><strong>Your subscription was updated</strong></a>
</div>
<div class="alert alert-alt">
Bryan Porter<small class="pull-right">10 min ago</small><br>
<a href="page_ready_inbox_message.html"><strong>A great opportunity</strong></a>
</div>
<div class="alert alert-alt">
Jose Duncan<small class="pull-right">30 min ago</small><br>
<a href="page_ready_inbox_message.html"><strong>Account Activation</strong></a>
</div>
<div class="alert alert-alt">
Henry Ellis<small class="pull-right">40 min ago</small><br>
<a href="page_ready_inbox_message.html"><strong>You reached 10.000 Followers!</strong></a>
</div>
</div>
<!-- END Messages -->
</div>
<!-- END Sidebar Content -->
</div>
<!-- END Wrapper for scrolling functionality -->
</div>
<!-- END Alternative Sidebar -->

<!-- Main Sidebar -->
<div id="sidebar">
<!-- Wrapper for scrolling functionality -->
<div id="sidebar-scroll">
<!-- Sidebar Content -->
<div class="sidebar-content">
<!-- Brand -->
<a href="http://www.mesportsagency.com.br" target="_blank" class="sidebar-brand">
<img src="public/img/favicon.png" width="10%" height="40%"><span class="sidebar-nav-mini-hide">|<strong>Entre no seu Site </strong></span>
</a>
<!-- END Brand -->

<!-- User Info -->
<div class="sidebar-section sidebar-user clearfix sidebar-nav-mini-hide">
<div class="sidebar-user-avatar">
<a href="page_ready_user_profile.html">
<img src="public/img/placeholders/avatars/avatar2.jpg" alt="avatar">
</a>
</div>
<div class="sidebar-user-name"><?php echo $_SESSION['userName']; ?></div>
<div class="sidebar-user-links">
<a href="page_ready_user_profile.html" data-toggle="tooltip" data-placement="bottom" title="Profile"><i class="gi gi-user"></i></a>
<a href="page_ready_inbox.html" data-toggle="tooltip" data-placement="bottom" title="Messages"><i class="gi gi-envelope"></i></a>
<!-- Opens the user settings modal that can be found at the bottom of each page (page_footer.html in PHP version) -->
<a href="javascript:void(0)" class="enable-tooltip" data-placement="bottom" title="Settings" onclick="$('#modal-user-settings').modal('show');"><i class="gi gi-cogwheel"></i></a>
<a href="logout" data-toggle="tooltip" data-placement="bottom" title="Logout"><i class="gi gi-exit"></i></a>
</div>
</div>
<!-- END User Info -->

<!-- Theme Colors -->
<!-- Change Color Theme functionality can be found in public/public/js/app.js - templateOptions() -->


<!-- Sidebar Navigation -->
<ul class="sidebar-nav">
<li>
<a href="index.html" class=" active"><i class="gi gi-home sidebar-nav-icon"></i><span class="sidebar-nav-mini-hide">início</span></a>
</li>


<!-- menu de cadastros --> 

<li class="sidebar-header">
<span class="sidebar-header-options clearfix"><a href="javascript:void(0)" data-toggle="tooltip" title="Quick Settings"></a><a href="javascript:void(0)" data-toggle="tooltip" title="Create the most amazing pages with the widget kit!"><i class="gi gi-user"></i></a></span>
<span class="sidebar-header-title">Cadastros </span>
</li>
<li>
<a href="lista-atletas"><i class="fa fa-user-plus sidebar-nav-icon"></i><span class="sidebar-nav-mini-hide">Atletas</span></a>
</li>
<li>
<a href="page_widgets_social.html"><i class="gi gi-old_man sidebar-nav-icon"></i><span class="sidebar-nav-mini-hide">Treinador</span></a>
</li>


<!-- fechamento do menu de cadastros --> 























</ul>

<!-- END Sidebar Navigation -->


</div>
<!-- END Sidebar Content -->
</div>
<!-- END Wrapper for scrolling functionality -->
</div>
<!-- END Main Sidebar -->

<!-- Main Container -->
<div id="main-container">



<header class="navbar navbar-default">
<!-- Left Header Navigation -->
<ul class="nav navbar-nav-custom">
<!-- Main Sidebar Toggle Button -->
<li>
<a href="javascript:void(0)" onclick="App.sidebar('toggle-sidebar');this.blur();">
<i class="fa fa-bars fa-fw"></i>
</a>
</li>
<!-- END Main Sidebar Toggle Button -->

<!-- Template Options -->
<!-- Change Options functionality can be found in public/js/app.js - templateOptions() -->

<!-- END Template Options -->
</ul>
<!-- END Left Header Navigation -->

<!-- Search Form -->
<form action="page_ready_search_results.html" method="post" class="navbar-form-custom">
<div class="form-group">

</div>
</form>
<!-- END Search Form -->


</header>    




<!-- Page content -->
<div id="page-content">
<!-- Dashboard Header -->
<!-- For an image header add the class 'content-header-media' and an image as in the following example -->
<div class="content-header content-header-media">
<div class="header-section">
<div class="row">

<!-- Main Title (hidden on small devices for the statistics to fit) -->
<div class="col-md-4 col-lg-6 hidden-xs hidden-sm">
<h1>Painel <strong>Administrativo</strong><br></h1>
<p>Seja bem vindo<strong>  <?php echo $_SESSION['userName'];?></strong></p>
</div>
<!-- END Main Title -->


<? date_default_timezone_set('America/Sao_Paulo');
$date = date('d/m/Y H:i'); ?>

<!-- Top Stats -->
<div class="col-md-8 col-lg-6">
<div class="row text-center">
<div class="col-xs-8 col-sm-12">
<h2 class="animation-hatch">
<strong>Data de hoje</strong><br>
<small><i class="fa"></i><? echo $date; ?></small>
</h2>


<?

$ip = getenv('HTTP_CLIENT_IP')?:
getenv('HTTP_X_FORWARDED_FOR')?:
getenv('HTTP_X_FORWARDED')?:
getenv('HTTP_FORWARDED_FOR')?:
getenv('HTTP_FORWARDED')?:
getenv('REMOTE_ADDR');


?>

<h2 class="animation-hatch">
<strong>IP</strong><br>
<small><i class="fa"></i><? echo $ip; ?></small>
</h2>
</div>





</div>
</div>
<!-- END Top Stats -->
</div>
</div>
<!-- For best results use an image with a resolution of 2560x248 pixels (You can also use a blurred image with ratio 10:1 - eg: 1000x100 pixels - it will adjust and look great!) -->
<img src="public/img/placeholders/headers/dashboard_header.jpg" alt="header image" class="animation-pulseSlow">
</div>
<!-- END Dashboard Header -->



<?php include $conteudo['page']; ?>











<!-- Footer -->
<footer class="clearfix">
<div class="pull-right">
Desenvolvido por: Victor Powilleit <i></i>
</div>
<div class="pull-left">
<span id=""> Todos os direitos reservados</span> &copy; 2019</a>
</div>
</footer>
<!-- END Footer -->
</div>
<!-- END Main Container -->
</div>
<!-- END Page Container -->
</div>
<!-- END Page Wrapper -->

<!-- Scroll to top link, initialized in public/js/app.js - scrollToTop() -->
<a href="#" id="to-top"><i class="fa fa-angle-double-up"></i></a>

<!-- User Settings, modal which opens from Settings link (found in top right user menu) and the Cog link (found in sidebar user info) -->
<div id="modal-user-settings" class="modal fade" tabindex="-1" role="dialog" aria-hidden="true">
<div class="modal-dialog">
<div class="modal-content">
<!-- Modal Header -->
<div class="modal-header text-center">
<h2 class="modal-title"><i class="fa fa-pencil"></i> Settings</h2>
</div>
<!-- END Modal Header -->

<!-- Modal Body -->
<div class="modal-body">
<form action="index.html" method="post" enctype="multipart/form-data" class="form-horizontal form-bordered" onsubmit="return false;">
<fieldset>
<legend>Vital Info</legend>
<div class="form-group">
<label class="col-md-4 control-label">Username</label>
<div class="col-md-8">
<p class="form-control-static">Admin</p>
</div>
</div>
<div class="form-group">
<label class="col-md-4 control-label" for="user-settings-email">Email</label>
<div class="col-md-8">
<input type="email" id="user-settings-email" name="user-settings-email" class="form-control" value="admin@example.com">
</div>
</div>
<div class="form-group">
<label class="col-md-4 control-label" for="user-settings-notifications">Email Notifications</label>
<div class="col-md-8">
<label class="switch switch-primary">
<input type="checkbox" id="user-settings-notifications" name="user-settings-notifications" value="1" checked>
<span></span>
</label>
</div>
</div>
</fieldset>
<fieldset>
<legend>Password Update</legend>
<div class="form-group">
<label class="col-md-4 control-label" for="user-settings-password">New Password</label>
<div class="col-md-8">
<input type="password" id="user-settings-password" name="user-settings-password" class="form-control" placeholder="Please choose a complex one..">
</div>
</div>
<div class="form-group">
<label class="col-md-4 control-label" for="user-settings-repassword">Confirm New Password</label>
<div class="col-md-8">
<input type="password" id="user-settings-repassword" name="user-settings-repassword" class="form-control" placeholder="..and confirm it!">
</div>
</div>
</fieldset>
<div class="form-group form-actions">
<div class="col-xs-12 text-right">
<button type="button" class="btn btn-sm btn-default" data-dismiss="modal">Close</button>
<button type="submit" class="btn btn-sm btn-primary">Save Changes</button>
</div>
</div>
</form>
</div>
<!-- END Modal Body -->
</div>
</div>
</div>


</body>
</html>

