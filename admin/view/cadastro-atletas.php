<?php 
session_start();
if( session_id() != ""){



?>

<!-- Page content -->
<div id="page-content">
<!-- Validation Header -->
<div class="content-header">
<div class="header-section">
<h1>
<i class="gi gi-server_plus"></i>Cadastro de Atletas<br><small>Formulario de castros de atletas</small>
</h1>
</div>
</div>


<ul class="breadcrumb breadcrumb-top">

</ul>
<!-- END Validation Header -->

<div class="row">
<div class="col-md-6">
<!-- Form Validation Example Block -->
<div class="block">
<!-- Form Validation Example Title -->
<div class="block-title">
<h2><strong>Informações</strong> do Atleta</h2>
</div>
<!-- END Form Validation Example Title -->

<!-- Form Validation Example Content -->
<form name="formatletas" action="lista-atletas"  method="POST" class="form-horizontal form-bordered" enctype="multipart/form-data">
<fieldset>



<div class="form-group">
<label class="col-md-4 control-label" for="plano_nome">Nome <span class="text-danger">*</span></label>
<div class="col-md-6">
<div class="input-group">
<input type="text" id="nome" name="nome" class="form-control" placeholder="Your username..">
<span class="input-group-addon"><i class="gi gi-user"></i></span>
</div>
</div>
</div>

<div class="form-group">
<label class="col-md-4 control-label" for="val_email">Nascimento<span class="text-danger">*</span></label>
<div class="col-md-6">
<div class="input-group">
<input type="text" id="example-datepicker" name="data_nasc" class="form-control input-datepicker" data-date-format="yyyy/mm/dd" placeholder="mm/dd/yy">
<span class="input-group-addon"><i class="fa fa-skyatlas"></i></span>
</div>
</div>
</div>

<div class="form-group">
<label class="col-md-4 control-label" for="posicao">Posição jogado<span class="text-danger">*</span></label>
<div class="col-md-6">
<div class="input-group">
<input type="text" id="text" name="posicao" class="form-control" placeholder="Posição jogado" >
<span class="input-group-addon"><i class="fa fa-skyatlas"></i></span>
</div>
</div>
</div>


<div class="form-group">
<label class="col-md-4 control-label" for="clube">Clube<span class="text-danger">*</span></label>
<div class="col-md-6">
<div class="input-group">
<input type="text" id="tranfer" name="clube" class="form-control" placeholder="Clube">
<span class="input-group-addon"><i class="fa fa-skyatlas"></i></span>
</div>
</div>
</div>

<div class="form-group">
<label class="col-md-4 control-label">Altura<span class="text-danger">*</span></label>
<div class="col-md-6">
<div class="input-group">
<input type="text" id="altura" name="altura" class="form-control" placeholder="link do video">
<span class="input-group-addon"><i class="fa fa-street-view"></i></span>
</div>
</div>
</div>


<div class="form-group">
<label class="col-md-4 control-label">Link<span class="text-danger">*</span></label>
<div class="col-md-6">
<div class="input-group">
<input type="text" id="link_video" name="link_video" class="form-control" placeholder="link do video">
<span class="input-group-addon"><i class="fa fa-youtube"></i></span>
</div>
</div>
</div>


</fieldset>



<div class="form-group form-actions">
<div class="col-md-8 col-md-offset-4">

</div>
</div>

</div>
<!-- END Validation Block -->
</div>



<!-- segundo asside --> 


<div class="col-md-6">
<!-- User Assist Block -->
<div class="block">
<!-- User Assist Title -->
<div class="block-title">
<h2><strong>Complementos</strong> das informações</h2>
</div>
<!-- END User Assist Title -->






<!-- User Assist Content -->
<div class="form-horizontal form-bordered">

<div class="form-group">

<?php
try {
$sql="SELECT * FROM pais ORDER BY paisname ASC";
$sth = $dbh->prepare($sql);
$sth->execute();
?>


<label class="col-md-4 control-label" for="example-text-input4">Nacionalidade</label>
<div class="col-md-6 ">

<select id="example-select2" name="nacionalidade" class="select-select2" style="width: 100%;">

<?php
foreach ($dbh->query($sql) as $row ) {
?>

<option value="<?php echo $row['paisid']; ?>">
<?php echo $row['paisname']; ?> </option>

<?php
}
?>

</select>
<?php

} catch (PDOException $e) {
print "Error!: " . $e->getMessage() . "<br/>";
die();
}
?>


</div>
</div>










<div class="form-group">
<label class="col-md-4 control-label" for="example-text-input4">Sexo</label>
<div class="col-md-6">
<select id="base_mysql" name="sexo"  class="form-control" size="1">
<option name="base_mysql" value="0">Masculino</option>
<option name="base_mysql" value="1">Feminino</option>

</select>
</div>
</div>



<div class="form-group">
<label class="col-md-4 control-label" for="example-text-input4">Status</label>
<div class="col-md-6">
<select id="example-select" name="status" class="form-control" size="1">
<option value="0">Disponivel</option>
<option value="1">Sob contrato</option>

</select>
</div>
</div>






<div class="form-group">
<label class="col-md-4 control-label" >Inicio<span class="text-danger">*</span></label>
<div class="col-md-6">
<div class="input-group">
<input type="text" id="preco" name="data_status_inicio" class="form-control" placeholder="Ano">
<span class="input-group-addon"><i class="fa fa-clock-o"></i></span>
</div>
</div>
</div>

<div class="form-group">
<label class="col-md-4 control-label" for="val_email">Fim<span class="text-danger">*</span></label>
<div class="col-md-6">
<div class="input-group">
<input type="text" id="preco" name="data_status_fim" class="form-control" placeholder="Ano">
<span class="input-group-addon"><i class="fa fa-clock-o"></i></span>
</div>
</div>
</div>

<div class="form-group ">
<label class="col-md-4 control-label">Foto<span class="text-danger">*</span></label>
<div class="col-md-6">
<div class="input-group">
<input type="file" name="atletas_img"/> 

</div>
</div>
</div>








<div class="form-group form-actions">
<div class="col-md-8 col-md-offset-5">

<input type="submit"class="btn btn-sm btn-primary" value="Cadastrar" >

</div>
</div>
</form>
<!-- END Form Validation Example Content -->
</div>
<!-- END User Assist Block -->


</div>
</div>
</div>









</div>
</div>











<? } ?>






