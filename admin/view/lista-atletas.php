<?
if( session_id() != ""){

	function upload($imageTemporary, $imageName, $width, $directoryName) {
$fileExtension = substr($imageName, -3);
switch($fileExtension){
case 'jpg':	$fileImage = imagecreatefromjpeg($imageTemporary); break;
case 'gif': $fileImage = imagecreatefromgif($imageTemporary); break;
case 'png':	$fileImage = imagecreatefrompng($imageTemporary); break;
}

$x = imagesx($fileImage);
$y = imagesy($fileImage);
$height = ($width*$y) / $x;
$newImage = imagecreatetruecolor($width, $height);

imagealphablending($newImage, false);
imagesavealpha($newImage, true);
imagecopyresampled($newImage, $fileImage, 0, 0, 0, 0, $width, $height, $x, $y);

switch($fileExtension){
case 'jpg': imagejpeg($newImage, $directoryName.$imageName, 100); break;
case 'gif': imagegif($newImage, $directoryName.$imageName); break;
case 'png': imagepng($newImage, $directoryName.$imageName); break;
}
imagedestroy($fileImage);
imagedestroy($newImage);

return true;

}


if (isset($_POST['nome']) &&  empty($_POST['nome']) == false) {

$nome = $_POST['nome'];
$sexo = $_POST['sexo'];
$data_nasc = $_POST['data_nasc'];
$posicao = $_POST['posicao'];
$altura = $_POST['altura'];
$nacionalidade = $_POST['nacionalidade'];
$clube = $_POST['clube'];
$status = $_POST['status'];
$data_status_inicio = $_POST['data_status_inicio'];
$data_status_fim = $_POST['data_status_fim'];
$link_video = $_POST['link_video'];
$atletas_img = $_POST['atletas_img'];

/**
* Inserção de imagem em diretorio
*/
$directoryName = "public/img/pastaatletas/";
$fileExtensionPermission = array('image/jpg', 'image/png', 'image/pjpeg', 'image/jpeg', 'image/gif');
$fileImage = $_FILES['atletas_img'];
if(!file_exists($directoryName))
mkdir($directoryName, 0755);

$imageTemporary    = $fileImage['tmp_name'];
$imageNameOriginal = $fileImage['name'];
$imageType         = $fileImage['type'];

if ( !empty($imageNameOriginal) && in_array( $imageType, $fileExtensionPermission ) ) {
switch($imageType){
case 'image/jpg'  : $fileExtension = '.jpg'; break;
case 'image/jpeg' : $fileExtension = '.jpg'; break;
case 'image/pjpeg': $fileExtension = '.jpg'; break;
case 'image/png'  : $fileExtension = '.png'; break;
case 'image/gif'  : $fileExtension = '.gif'; break;
}

$atletas_img = 'gt-' .md5(uniqid(rand(), true)) .$fileExtension;

if(upload($imageTemporary, $atletas_img, 800, $directoryName) == true){



/**
* Cadastros atleta
*/

try {

$sql = $dbh->prepare ("INSERT INTO atletas SET nome = :nome, sexo =:sexo, data_nasc = :data_nasc, posicao = :posicao, altura = :altura, nacionalidade = :nacionalidade, clube = :clube, status = :status, data_status_inicio = :data_status_inicio, data_status_fim = :data_status_fim, link_video = :link_video, atletas_img = :atletas_img");


$sql->bindValue(":nome",$nome);
$sql->bindValue(":sexo",$sexo);
$sql->bindValue(":data_nasc",$data_nasc);
$sql->bindValue(":posicao",$posicao);
$sql->bindValue(":altura",$altura);
$sql->bindValue(":nacionalidade",$nacionalidade);
$sql->bindValue(":clube",$clube);
$sql->bindValue(":status",$status);
$sql->bindValue(":data_status_inicio",$data_status_inicio);
$sql->bindValue(":data_status_fim",$data_status_fim);
$sql->bindValue(":link_video",$link_video);
$sql->bindValue(":atletas_img",$atletas_img);
$sql->execute();


	} catch (PDOException $e) {
	print "Error!: " . $e->getMessage() . "<br/>";
		die();
		}
	};

} else {
$messageReturn = 'Tipo do arquivo não aceito, favor enviar apenas imagens válidas!';
		}
	}

	


?>

<!-- Page content -->
<div id="page-content">
<!-- Validation Header -->
<div class="content-header">
<div class="header-section">
<h1>
<i class="gi gi-server_new"></i>Painel de Plano de Host<br><small>Essa aba adicionamos e fazemos toda a rotina de planos de hopedagem para os nosso clientes</small>
</h1>
</div>
</div>


<div class="block full">
<div class="table-responsive">

<div class="table-responsive">
<table id="example-datatable" class="table table-vcenter table-condensed table-bordered">

<thead>
<a href="cadastro-atletas" class="btn btn-primary">Adicionar Atletas</a>
<tr>


<th class="text-center">Cod do Atleta</th>
<th class="text-center">Nome</th>
<th>Sexo</th>
<th>Posição</th>
<th>Clube</th>
<th>Foto</th>
<th class="text-center">Ação</th>
</tr>
</thead>
<tbody>

<?php



try {
$sql = "SELECT * FROM atletas";
$sth = $dbh->prepare($sql);
$sth->execute();

foreach( $dbh->query($sql) as $row ) {
?>





<tr>
<td class="text-center"><?echo $row['id_atletas']; ?></td>
<td class="text-center"><?echo $row['nome'] ?> </td>
<td class="text-center"><? echo $row['sexo']== 0 ? 'Masculino':'Feminino'; ?></td>
<td class="text-center"><span class="label label-info"><? echo $row['posicao'];?> </span></td>
<td class="text-center"><? echo $row['clube']; ?></td>
<td><img src="public/img/pastaatletas/<?php echo $row['atletas_img']; ?>" width="50px"></td>
<td class="text-center">
<div class="btn-group">

	<form class="btn-group" action="editar-host" method="POST">

		<input type="hidden" id="hostid" name="hostid" value=" <?php echo $row['id_planos']; ?>">

		<button class="btn btn-xs btn-default" title="Editar"><i class="fa fa-pencil"></i></button>


</form>

<a href="javascript:void(0)" data-toggle="tooltip" title="Delete" class="btn btn-xs btn-danger"><i class="fa fa-times"></i></a>
</div>

</td>
</tr>


<?php
        }

    } catch (PDOException $e) {
        print "Error!: " . $e->getMessage() . "<br/>";
        die();
    }
    ?>


</tbody>




</table>

<!-- END Datatables Content -->



</div>
</div>
</div>
</div>

</div>







<? } ?>
